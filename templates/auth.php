

<div class="container" ng-controller='LoginController'>
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="logos">
			
			<a href="/" class="logo">
				<img src="img/logo.png" alt="">
			</a>
			<h4 class="title_after_logo">Онлайн курсы</h4>

		</div>

	
		<form ng-submit="loginSubmit($event)" class="auth-form clearfix">

			<div class="col-md-12 col-xs-12 clearfix">
				<div class="form-notif">
					<h4>Укажите ваши E-mail и пароль для входа:</h4>
				</div>
			</div>
			
			<div class="col-md-offset-1 col-md-4 col-xs-12 clearfix">
				<input type="text" ng-model="loginFormData.email" name="email" placeholder="E-mail" required>
			</div>

			<div class="col-md-4 col-xs-12 clearfix">
				<div class="in-input">
					<input ng-model="loginFormData.password" type="password" name="password" placeholder="Пароль" required>
						<a class="forget" href="forget">Забыли?</a>
				</div>
			</div>

			<div class="col-md-2 col-xs-12 clearfix">
				<button class="btn btn-auth" type="submit">Войти</button>
			</div>

		</form>

	</div>

</div>