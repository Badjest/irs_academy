<? $data = $arrayOfData;?>


<div class="container" ng-controller="AdminUserId" ng-init="init(<?php echo htmlspecialchars(json_encode($data['data_user'])); ?>,
<?php echo htmlspecialchars(json_encode($data['cources'])); ?>)">

	<div class="col-md-12 col-sm-12 col-xs-12">

		<a href="" ng-click="saveSet($event)" class="btn fly-save">Сохранить <br> изменения</a>
		
		<div class="default-form clearfix">

			<div class="col-md-12 col-xs-12 clearfix">
				<h3 class="def-title">Профиль</h3>
			</div>
			
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.mail" placeholder="E-mail">
			</div>

			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.username" placeholder="Имя">
			</div>

			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.userlastname" placeholder="Фамилия">
			</div>
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.phone" placeholder="Контактный телефон">
			</div>
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.vk" placeholder="Профиль Vkontakte">
			</div>





		</div>

        <div ng-if="userData.block === '0'" class="block-div">
            <button ng-click="blockUser('1')" class="btn red">Заблокировать пользователя</button>
        </div>
        <div ng-if="userData.block === '1'" class="block-div">
            <button ng-click="blockUser('0')" class="btn">Разблокировать пользователя</button>
        </div>

		<div class="default-form clearfix">

			<div class="col-md-12 col-xs-12 clearfix">
				<h3 class="def-title">Доступные курсы</h3>
			</div>
		</div>



        


		<div class="admin-user-cource-in-user-edit" ng-repeat="item in cources track by $index" ng-init="parentIndex = $index">
			<input style="margin-top: 15px;" type="checkbox" ng-true-value="1"
				   ng-false-value="0"
				   ng-model="item.his_cource" id="bla-{{$index}}" />
			<label style="margin-top: 30px;" for="bla-{{$index}}">{{item.name}}</label>

			<div ng-if="item.his_cource == 1" class="admin-lessons-user">

				<div class="admin-lessons-user-item"
					 ng-repeat="lesson in item.lessons | orderBy:'sort'">
					<input style="margin-top: 15px;" type="checkbox" ng-true-value="'1'"
						   ng-false-value="'0'"
						   ng-model="lesson.pay" id="admin-b-{{ $parent.$index }}-{{$index}}" />
					<label style="margin-top: 30px;" for="admin-b-{{ $parent.$index }}-{{$index}}">{{lesson.name}}</label>
				</div>

			</div>

            <div ng-click='openAllLessons(item.id)' class="btn">
                Открыть все уроки данного курса
            </div>

		</div>


	</div>

</div>