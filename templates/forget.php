
<div class="container">
	
	<div class="col-md-12 col-sm-12 col-xs-12 clearfix" ng-controller="forgetPass">
		
		<div class="logos">
			
			<a href="<?=ROOT?>/" class="logo">
				<img src="<?=BASE_LINK?>/img/logo.png" alt="">
			</a>
			<h4 class="title_after_logo">Онлайн курсы</h4>

		</div>

	
		<form ng-submit="passRecover($event)" action="" class="forget-form clearfix">

			<div class="col-md-12 col-xs-12 clearfix">
				<div class="form-notif">
					<h3>Восстановление доступа</h3>
					<h5>Введите E-mail адрес для восстановления доступа к сервису</h5>
				</div>
			</div>
			
			<div class="clearfix">
				<div class="col-md-offset-4 col-md-4 col-xs-12 clearfix">
					<input type="text" ng-model="mail" name="email" placeholder="E-mail">
				</div>
			</div>

			<div class="col-md-offset-4 col-md-4 col-xs-12 clearfix" >
				<button type="submit" class="btn btn-reg">Восстановить пароль</button>
			</div>

		</form>

	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12 clearfix">
		<div class="block-with-single-link">
			<a href="auth">Авторизоваться</a>
		</div>
	</div>	

</div>