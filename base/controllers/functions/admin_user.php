<?

function admin_users() {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $search_word = $_POST['word'];

    $limit = 40;
    $page = (isset($_GET['page']) ? $_GET['page'] : 1 );
    $offset = $limit * ($page - 1);

    $orderby = ($_GET['orderby'] ? $_GET['orderby'] : 'id');

    if ($search_word) {
        $users = $db->select('Select * from users WHERE username LIKE "%'.$search_word.'%" or mail LIKE "%'.$search_word.'%" OR userlastname LIKE "%'.$search_word.'%" ORDER BY '.$orderby.' DESC');
    } else {
        $users = $db->select('Select * from users ORDER BY '.$orderby.' DESC LIMIT '.$limit.' OFFSET '. $offset);
    }


    $count_pages = $db->select('select COUNT(id) as c FROM users');
    $count_pages = ceil((int)$count_pages[0]['c'] / $limit);


    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Управление пользователями',
            'username' => $username
        )
    );
    echo loadView('templates/admin_user.php',array(
        'users'=> $users,
        'count_pages' => $count_pages
    ));
    echo loadView('templates/footer.php',array());
}