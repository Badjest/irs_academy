<!DOCTYPE html>
<html lang="ru" ng-app="app" ng-controller="PublicController" ng-init="init('<?=ROOT?>','<?=BASE_LINK?>')">
<head>
	<? $data = $arrayOfData;?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">



	<title><?=$data['title'];?></title>
	<link rel="stylesheet" href="<?=BASE_LINK?>/libs/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?=BASE_LINK?>/libs/bootstrap/css/bootstrap-theme.min.css" />
	

	<link rel="stylesheet" href="<?=BASE_LINK?>/libs/redactor/redactor/redactor.css" />
	<link rel="stylesheet" href="<?=BASE_LINK?>/libs/simplebar-2.4.4/dist/simplebar.css" />


	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?=BASE_LINK?>/css/style.css" />


	<? if ($data['chat_on']) {?>
	<?/*?><script src="https://irs.academy:8091/socket.io/socket.io.js"></script><?*/?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
	<? } ?>

    <? if ($data['timer']) {?>
        <script src="<?=BASE_LINK?>/libs/syotimer/jquery.js"></script>
        <script src="<?=BASE_LINK?>/libs/syotimer/jquery.syotimer.min.js"></script>

    <? } ?>

<!--	<link rel="stylesheet/less" type="text/css" href="http://xn--90ana5af.xn--p1ai<?=ROOT?>/css/media.less" />-->
<!--	<script src="http://xn--90ana5af.xn--p1ai<?=ROOT?>/libs/less/less.js"></script>-->
</head>

<body>



<? if ($data['show_header']) {?>
	<header class="clearfix">



		<div class="col-md-3 col-xs-12">
			<a class="logo-header" href="<?=ROOT?>/">
				<img src="<?=BASE_LINK?>/img/logo.png" alt="">
			</a>
			<h4 class="title_right_logo">Онлайн курсы</h4>
		</div>

		<div class="col-md-4 col-xs-12">
			<div class="nav-cource-panel">
				<ul>
					<li>
						<a class="current" href="<?=ROOT?>/my">Мои курсы</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="col-md-5 col-xs-12">
			<div class="right-side">
				
				<div class="nav-block">
					<span id='yourName'><?=$data['username']?></span>
					<span class="treug"></span>
					<ul class="menu" ng-controller='HeaderController'>
						<li><a href="<?=ROOT?>/my">Мои курсы</a></li>
						<li><a href="<?=ROOT?>/settings">Профиль</a></li>
						<li><a ng-click="exitUser($event)" href="">Выход</a></li>
					</ul>
				</div>

				<div title="Количество баллов" class="nav-block your-score">
					<img class="your-score-img" src="<?=BASE_LINK?>/img/brilliant.png" alt="Количество баллов" title="Количество баллов" >
					<span title="Количество баллов"><?=get_my_score(); ?></span>
				</div>
			</div>
		</div>

	</header>
<? } ?>
	
	<div class="loader">
		<div class="line"></div>	
	</div>