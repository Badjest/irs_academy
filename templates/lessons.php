<? $data = $arrayOfData;?>

<div class="container" ng-controller="LessonsController"
	 ng-init="init(<?php echo htmlspecialchars(json_encode($data['lessons'])); ?>,<?php echo htmlspecialchars(json_encode($data['cource'])); ?>)">
	
	<div class="col-md-12 col-sm-12 col-xs-12">


		<div class="my-courses">
			
			<div class="course-item mini clearfix">
				
				<div class="col-md-12 col-xs-12">
					<img src="{{ cource.photo }}" alt="">
					<span class="courceTitle">{{ cource.name }}</span>
				</div>

				<div class="col-md-12 col-xs-12">
					<h4>Пройдено уроков: {{ data.complete }} из {{ data.all }}</h4>
					<div class="project-process">
						<div class="line">
						</div>
					</div>
				</div>


			</div>

			<h3 class="def-title">Список уроков</h3>


			<a ng-repeat="item in lessons track by $index" href="<?=ROOT?>/lesson/{{item.id}}" class="course-item mini clearfix">
				
				<div class="col-md-7 col-xs-12">
					<div class="col-md-3">
						<img src="{{ item.photo }}" alt="">
					</div>
					<div class="col-md-9">
						<span class="lesson-name" ng-bind-html="item.name"> </span>
					</div>


				</div>

				<div class="col-md-2 col-xs-12">
					<div class="right-side-item">
						<div ng-if="item.stage == 'ACCESS'" class="status st-access">
							<span class="access">Зачет</span>
						</div>
						<div ng-if="item.stage == 'NEW'" class="status st-new">
							<span class="new">Новое</span>
						</div>
						<div ng-if="item.stage == 'CLOSE'" class="status">
							<span class="lock">Недоступно</span>
						</div>
						<div ng-if="item.stage == 'FAIL'" class="status">
							<span class="fail">Не зачёт</span>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-xs-12">
					<div class="right-side-item">
						Время выполнения:<br>
						<span id='timeBack'>{{ item.timefor }}</span>
					</div>
				</div>

			</a>



		</div>

	</div>

</div>