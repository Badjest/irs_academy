<?
function webinar($url) {

    $utm_source = $_GET['utm_source'];
    $utm_medium = $_GET['utm_medium'];
    $utm_campaign = $_GET['utm_campaign'];
    $utm_content = $_GET['utm_content'];


    if (!isset($_COOKIE['utm_source']) && isset($utm_content)) {
        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.update.json';
        $queryData = http_build_query(array(
            'id' => $utm_content,
            'fields' => array(
                "STAGE_ID" => 'C34:PREPARATION',
                "UF_CRM_CT_UTM_SOUR" => $utm_source,
                "UF_CRM_CT_UTM_MEDI" => $utm_medium,
                "UF_CRM_CT_UTM_CAMP" => $utm_campaign,
                "UF_CRM_CT_UTM_CONT" => $utm_content
            ),
        ));
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        curl_exec($curl);
        curl_close($curl);

        setcookie("utm_source", $utm_source, time()+36000000000);
        setcookie("utm_medium", $utm_medium, time()+36000000000);
        setcookie("utm_campaign", $utm_campaign, time()+36000000000);
        setcookie("utm_content", $utm_content, time()+36000000000);
    }


    $db = get_db();
    $webinar = $db->select('select * from webinars where url = {?}', array($url));
    if (count($webinar) == 0) {
        header('Location: '.ROOT.'/');
        die();
    }

    echo loadView('templates/header.php',
        array(
            'show_header'=>false,
            'title' => $webinar[0]['name'],
            'timer' => true
        )
    );

    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {
            $admin = false;
        } else {
            $admin = true;
        }
    }

    $id_webinar = $webinar[0]['id'];

    if ($admin) {
        $messages = $db->select('select * from webinars_message where id_webinar = {?} ORDER BY webinars_message.time ASC', array($id_webinar));
    } else {
        $messages = array();
    }

    $count_of_people = rand(50, 400);

    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Novosibirsk'));
    $date = $date->format("H:i:s");

    $webinar[0]['date_now'] = $date;

    $names = $db->select('SELECT webinars_message.name from webinars_message 
GROUP BY webinars_message.name
ORDER BY RAND()
LIMIT 8');

    echo loadView('templates/webinar.php',array(
        'webinar' => $webinar[0],
        'admin' => $admin,
        'messages' => $messages,
        'count_of_people' => $count_of_people,
        'names' => $names
    ));
    echo loadView('templates/footer.php',array(

    ));
}