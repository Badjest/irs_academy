<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

date_default_timezone_set('Asia/Novosibirsk');

//require_once('../classes/VK/VK.php');
//require_once('../classes/VK/VKException.php');
//require_once('../classes/DB/Db.php');
//$config_db = include "../config/config_db.php";

//require_once  '/academy/base/classes/VK/VK.php';
//require_once  '/academy/base/classes/VK/VKException.php';
//require_once  '/academy/base/classes/DB/Db.php';

$config_db = include "/academy/base/config/config_db.php";

$db = Db::getDB($config_db);


$vk = new VK('{6189467}', '{jDQ82PPRfIU9yLP7ztHj}');


// время вебинара
$date_web = '23:59:59';

// время за час до вебинара
$date_hour = '21:00:00';

// время за 5 минут до вебинара
$date_minute = '22:55:00';


$accessTokens = array(
    0 => array(
        'tokens' => array(
            0 => 'c9d248522c9ccb9639bddc3e5f9cbbf374e83767eeaa211feb56279fa1441f72dac0c38e5ee0d7c19b637')
    ,
        'uid' => '427511680',
        'assigned' =>  910
    ),
    1 => array(
        'tokens' => array(
            0 => 'cb7c130eaade0c57a6987acbb4423abb0a44a62e71760a596d408611e2852ce96e07331b7427d604ddec2',
            1 => '365be758cad8b080c9f4c05c2c6c70f6b0f004019078c4e2c300596c3bf802b56d1b0603aa9045056aa9e'
        ),
        'uid' => '442379237',
        'assigned' => 538
    ),
    2 => array(
        'tokens' =>  array(
            0 => '6687f4703824b123addbfe9f19306195770a841e44b35c13be34ba7bd20f303c29c16ebfbc4117f7966be'
        ),
        'uid'=>'175204143',
        'assigned' => 908
    )
);


$all_users = $db->select('select id_person , id_deal from users_for_webinar', array());
$exclude_users = $db->select('select  id_user from exclude_webinar_user', array());

$exclude_ids = array();

foreach ($exclude_users as $value) {
    $exclude_ids[] = $value['id_user'];
}

$start = 0;
$resultDeals = array();

$queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.list.json';

$queryData = http_build_query(array(
    'order' =>  array(),
    'filter' => array(
        "CATEGORY_ID" => 14,
        'STAGE_ID' => array('C14:LOSE'),
        "!=CONTACT_ID" => $exclude_ids
    ),
    'select' => array("ID", "TITLE", "STAGE_ID", "CONTACT_ID"),
));

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);

curl_close($curl);
$result = json_decode($result, 1);
$resultDeals = $result['result'];


$ids = array();

// формируем массив с id контактов и заносим id пользователей в базу, чтобы больше их не вытаскивать
foreach ($resultDeals as $value) {
    $db->query('insert into exclude_webinar_user set id_user = {?}', array($value['CONTACT_ID']));
    $ids[] = $value['CONTACT_ID'];
}

$resultContacts = array();

if (count($ids) > 0) {


// выбираем всех контактов с совпавшими id

    $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.contact.list.json';

    $queryData = http_build_query(array(
        'order' =>  array(),
        'filter' => array( 'ID' =>  $ids),
        'select' => array("ID", "NAME", "WEB" , "ASSIGNED_BY_ID")
    ));


    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

    $web = curl_exec($curl);
    curl_close($curl);
    $web = json_decode($web, 1);

    $resultContacts = $web['result'];

}





// крч, на этом этапе мы вытащили всех отказавшихся чуваков, вытащили их id, имя, вк
// теперь нужно создать сделки в воронке автовебинар с этими типами
// также заносим типов в базу данных

// цикл по чувакам
foreach ($resultContacts as $value) {

    // количество чуваков в бд с таким id
    $key = array_search((int)$value['ID'], array_column($all_users, 'id_person'));


    // если ни одного заносим в БД и создаем сделку в воронке автовебинаров по данному типу
    if (!$key && is_bool($key)) {

        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.add.json';
        $queryData = http_build_query(array(
            'fields' => array(
                "TITLE" => $value['NAME'] . ' (из ВК)',
                "CURRENCY_ID"=> "RUB",
                "OPENED" => "Y",
                "OPPORTUNITY" => 0,
                "STAGE_ID" => 'C34:NEW',
                "CONTACT_ID" => $value['ID'],
                "CATEGORY_ID" => 34,
                "ASSIGNED_BY_ID" => $value['ASSIGNED_BY_ID'],
                "UF_CRM_1502353695" => 144
            )
        ));
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);


        $deal_ID = json_decode($result, 1);
        $deal_ID = $deal_ID["result"];
        $pos = strrpos($value['WEB'][0]['VALUE'], 'id');

        if (is_numeric($deal_ID) && !is_bool($pos) ) {
            $db->query('insert into users_for_webinar
        set name = {?},
        id_person = {?},
        assigned = {?},
        send_first = 0,
        send_second = 0,
        send_third = 0,
        vk = {?},
        id_deal = {?}',
                array($value['NAME'],
                    $value['ID'],
                    $value['ASSIGNED_BY_ID'],
                    mb_strcut($value['WEB'][0]['VALUE'],2) ,
                    $deal_ID ));
        }

    }

}


// вытащим всех чуваков, которые удовлетворяют условию
// первое сообщение не отправлено

$all_first_no_send = $db->select('select * from users_for_webinar where send_first = 0 LIMIT 20');




foreach ($all_first_no_send as $value) {
    $key_number = array_search((int)$value['assigned'], array_column($accessTokens, 'assigned'));

    if (is_int($key_number)) {

        // здесь первоначально проверяем
        // является ли другом ответсвеный и пользователь


        $message_one = 'Сегодня я провожу увлекательный, бесплатный вебинар😃 <br><br>
    В 20-00 по МСК в приятной, уютной компании поделюсь некоторыми секретами, полезными для Вас.<br><br>Ровно в 20-00 по МСК жду вас по ссылке на прямую трансляцию -> https://irs.academy/webinar/retush?utm_content='.$value['id_deal'].'&utm_source=vk&utm_medium='.$value['assigned'].'&utm_campaign=vksales <br><br>Не пропустите, записи не будет!';


        if (sendMessage($vk, $value, $accessTokens, $message_one, $key_number)) {
            $db->query('update users_for_webinar set send_first = 1 where id = {?}',array($value['id']));
            break;
        } else {
            // если не удалось отправить сообщение пользователю, статус 3
            $db->query('update users_for_webinar set send_first = 3 , send_second = 3 where id = {?}',array($value['id']));
        }

    } else {
        $db->query('update users_for_webinar set send_first = 3 , send_second = 3 where id = {?}',array($value['id']));
    }
}


// вытащим всех чуваков, которые удовлетворяют условию
// второе сообщение не отправлено

$date = new DateTime();

$date->setTimezone(new DateTimeZone('Asia/Novosibirsk'));
$date_now = $date->format("H:i:s");


// чекаем временной промежуток

if ($date_now >= $date_hour && $date_now < $date_web ) {

    $all_second_no_send = $db->select('select * from users_for_webinar where send_second = 0 LIMIT 20');

    foreach ($all_second_no_send as $value) {
        $key_number = array_search((int)$value['assigned'], array_column($accessTokens, 'assigned'));
        if (is_int($key_number)) {

            $message_two = 'Напоминаю вам, сегодня ровно в 20-00 по МСК жду вас по ссылке на прямую трансляцию ->
      https://irs.academy/webinar/retush?utm_content='.$value['id_deal'].'&utm_source=vk&utm_medium='.$value['assigned'].'&utm_campaign=vksales
      ';

            // здесь первоначально проверяем
            // является ли другом ответсвеный и пользователь

            if (sendMessage($vk, $value, $accessTokens, $message_two, $key_number)) {
                $db->query('update users_for_webinar set send_second = 1 where id = {?}',array($value['id']));
                break;
            }
        }
    }
}



echo 'ok';

$file = __DIR__ . '/log.txt';

$current = file_get_contents($file);

$date = new DateTime();

$date->setTimezone(new DateTimeZone('Asia/Novosibirsk'));
$date_now = $date->format("H:i:s Y-m-d");

$current .= $date_now .  " Скрипт выполнился\n";

file_put_contents($file, $current);

function sendMessage($vk, $value, $accessTokens, $message, $key_number) {

    for ($i=0; $i < count($accessTokens[$key_number]['tokens']); $i++) {
        $vk->setAccessToken($accessTokens[$key_number]['tokens'][$i]);

        $is_friend = $vk->api('friends.areFriends', array(
            'user_ids'   =>  $value['vk']
        ));


        if ($is_friend['response'][0]['friend_status'] === 3) {

            $send_message = $vk->api('messages.send', array(
                'user_ids'   =>  $value['vk'],
                'message' => $message
            ));

            if ($send_message['response'][0]) {
                return true;
            } else {
                var_dump($send_message);
                continue;
            }

        }

    }

    return false;


}


class Db {

    private static $db = null;
    private $mysqli;
    private $sym_query = "{?}";

    public static function getDB($config) {
        if (self::$db == null) self::$db = new Db($config);
        return self::$db;
    }

    private function __construct($config) {
        $this->mysqli = new mysqli($config->host ,$config->user , $config->password, $config->dbname);
        $this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    private function getQuery($query, $params) {
        if ($params) {
            for ($i = 0; $i < count($params); $i++) {
                $pos = strpos($query, $this->sym_query);
                $arg = "'".$this->mysqli->real_escape_string($params[$i])."'";
                $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
            }
        }
        return $query;
    }

    public function select($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if (!$result_set) return  $this->mysqli->error;
        return $this->resultSetToArray($result_set);
    }

    public function query($query, $params = false) {
        $success = $this->mysqli->query($this->getQuery($query, $params));
        if ($success) {
            if ($this->mysqli->insert_id === 0) return true;
            else return $this->mysqli->insert_id;
        }
        else return $this->mysqli->error;
    }

    private function resultSetToArray($result_set) {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = $row;
        }
        return $array;
    }

    public function __destruct() {
        if ($this->mysqli) $this->mysqli->close();
    }

}


class VK
{
    /**
     * VK application id.
     * @var string
     */
    private $app_id;
    /**
     * VK application secret key.
     * @var string
     */
    private $api_secret;
    /**
     * API version. If null uses latest version.
     * @var int
     */
    private $api_version;
    /**
     * VK access token.
     * @var string
     */
    private $access_token;
    /**
     * Authorization status.
     * @var bool
     */
    private $auth = false;
    /**
     * Instance curl.
     * @var Resource
     */
    private $ch;
    const AUTHORIZE_URL = 'https://oauth.vk.com/authorize';
    const ACCESS_TOKEN_URL = 'https://oauth.vk.com/access_token';
    /**
     * Constructor.
     * @param   string $app_id
     * @param   string $api_secret
     * @param   string $access_token
     * @throws  VKException
     */
    public function __construct($app_id, $api_secret, $access_token = null)
    {
        $this->app_id = $app_id;
        $this->api_secret = $api_secret;
        $this->setAccessToken($access_token);
        $this->ch = curl_init();
    }
    /**
     * Destructor.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }
    /**
     * Set special API version.
     * @param   int $version
     * @return  void
     */
    public function setApiVersion($version)
    {
        $this->api_version = $version;
    }
    /**
     * Set Access Token.
     * @param   string $access_token
     * @throws  VKException
     * @return  void
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }
    /**
     * Returns base API url.
     * @param   string $method
     * @param   string $response_format
     * @return  string
     */
    public function getApiUrl($method, $response_format = 'json')
    {
        return 'https://api.vk.com/method/' . $method . '.' . $response_format;
    }
    /**
     * Returns authorization link with passed parameters.
     * @param   string $api_settings
     * @param   string $callback_url
     * @param   bool $test_mode
     * @return  string
     */
    public function getAuthorizeUrl($api_settings = '',
                                    $callback_url = 'https://api.vk.com/blank.html', $test_mode = false)
    {
        $parameters = array(
            'client_id' => $this->app_id,
            'scope' => $api_settings,
            'redirect_uri' => $callback_url,
            'response_type' => 'code'
        );
        if ($test_mode)
            $parameters['test_mode'] = 1;
        return $this->createUrl(self::AUTHORIZE_URL, $parameters);
    }
    /**
     * Returns access token by code received on authorization link.
     * @param   string $code
     * @param   string $callback_url
     * @throws  VKException
     * @return  array
     */
    public function getAccessToken($code, $callback_url = 'https://api.vk.com/blank.html')
    {
        if (!is_null($this->access_token) && $this->auth) {
            throw new VKException('Already authorized.');
        }
        $parameters = array(
            'client_id' => $this->app_id,
            'client_secret' => $this->api_secret,
            'code' => $code,
            'redirect_uri' => $callback_url
        );
        $rs = json_decode($this->request(
            $this->createUrl(self::ACCESS_TOKEN_URL, $parameters)), true);
        if (isset($rs['error'])) {
            throw new VKException($rs['error'] .
                (!isset($rs['error_description']) ?: ': ' . $rs['error_description']));
        } else {
            $this->auth = true;
            $this->access_token = $rs['access_token'];
            return $rs;
        }
    }
    /**
     * Return user authorization status.
     * @return  bool
     */
    public function isAuth()
    {
        return !is_null($this->access_token);
    }
    /**
     * Check for validity access token.
     * @param   string $access_token
     * @return  bool
     */
    public function checkAccessToken($access_token = null)
    {
        $token = is_null($access_token) ? $this->access_token : $access_token;
        if (is_null($token)) return false;
        $rs = $this->api('getUserSettings', array('access_token' => $token));
        return isset($rs['response']);
    }
    /**
     * Execute API method with parameters and return result.
     * @param   string $method
     * @param   array $parameters
     * @param   string $format
     * @param   string $requestMethod
     * @return  mixed
     */
    public function api($method, $parameters = array(), $format = 'array', $requestMethod = 'get')
    {
        $parameters['timestamp'] = time();
        $parameters['api_id'] = $this->app_id;
        $parameters['random'] = rand(0, 10000);
        if (!array_key_exists('access_token', $parameters) && !is_null($this->access_token)) {
            $parameters['access_token'] = $this->access_token;
        }
        if (!array_key_exists('v', $parameters) && !is_null($this->api_version)) {
            $parameters['v'] = $this->api_version;
        }
        ksort($parameters);
        $sig = '';
        foreach ($parameters as $key => $value) {
            $sig .= $key . '=' . $value;
        }
        $sig .= $this->api_secret;
        $parameters['sig'] = md5($sig);
        if ($method == 'execute' || $requestMethod == 'post') {
            $rs = $this->request(
                $this->getApiUrl($method, $format == 'array' ? 'json' : $format), "POST", $parameters);
        } else {
            $rs = $this->request($this->createUrl(
                $this->getApiUrl($method, $format == 'array' ? 'json' : $format), $parameters));
        }
        return $format == 'array' ? json_decode($rs, true) : $rs;
    }
    /**
     * Concatenate keys and values to url format and return url.
     * @param   string $url
     * @param   array $parameters
     * @return  string
     */
    private function createUrl($url, $parameters)
    {
        $url .= '?' . http_build_query($parameters);
        return $url;
    }
    /**
     * Executes request on link.
     * @param   string $url
     * @param   string $method
     * @param   array $postfields
     * @return  string
     */
    private function request($url, $method = 'GET', $postfields = array())
    {
        curl_setopt_array($this->ch, array(
            CURLOPT_USERAGENT => 'VK/1.0 (+https://github.com/vladkens/VK))',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST => ($method == 'POST'),
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_URL => $url
        ));
        return curl_exec($this->ch);
    }
}
;


class VKException extends \Exception {  }
