<?

/**
 * Функция возвращает общее количесво баллов за домашние работы
 * */
function get_my_score() {
    
    $hash = $_COOKIE['hash'];
    $db = get_db();
    $id_user = $db->select('select id from users where hash={?}', array($hash))[0]['id'];

    $marks = $db->select('SELECT MAX(homeworks.mark) as max_mark , homeworks.id_user_lesson 
    	FROM homeworks JOIN user_lessons on homeworks.id_user_lesson = user_lessons.id 
    	WHERE user_lessons.id_user = {?}
    	GROUP BY homeworks.id_user_lesson', array($id_user));

    $sum = 0;
    foreach ($marks as $value) {
    	$sum += $value['max_mark'];
    }
   
    return $sum;
}