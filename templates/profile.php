<? $data = $arrayOfData;?>

<script type="text/javascript" src="https://vk.com/js/api/openapi.js?151"></script>

<div class="container" ng-controller="SettingController"
     ng-init="init(<?php echo htmlspecialchars(json_encode($data['data_user'])); ?>)">

	<div class="col-md-12 col-sm-12 col-xs-12">

		<? if ($data['data_user']['admin']) {?>
		<div><a href="<?=ROOT?>/admin_cource"  class="btn btn-access">Редактировать курсы</a></div>
		<div><a href="<?=ROOT?>/admin_users"  class="btn btn-access">Редактировать пользователей</a></div>
			<div><a href="<?=ROOT?>/homeworks"  class="btn btn-access">Домашние задания</a></div>
			<div><a href="<?=ROOT?>/messages"  class="btn btn-access">Сообщения</a></div>
            <div><a href="<?=ROOT?>/scores"  class="btn btn-access">Рейтинги</a></div>
		<? } ?>
		
		<div class="default-form clearfix">

			<div class="col-md-12 col-xs-12 clearfix">
				<h3 class="def-title">Профиль</h3>
			</div>
			
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" disabled name="" ng-model="userData.mail" placeholder="E-mail">
			</div>

			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" disabled name="" ng-model="userData.username" placeholder="Имя">
			</div>

			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.userlastname" placeholder="Фамилия">
			</div>
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.phone" placeholder="Контактный телефон">
			</div>
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="text" name="" ng-model="userData.vk" disabled placeholder="Профиль Vkontakte">
			</div>


		</div>


		<div class="default-form clearfix">

			<div class="col-md-12 col-xs-12 clearfix">
				<h3 class="def-title">Настройки безопасности</h3>
			</div>
			
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="password" name="" ng-model="passwords.new" placeholder="Новый пароль">
			</div>
			<div class="col-md-4 col-xs-12 clearfix">
				<input type="password" name="" id="secondPassField" ng-change="confirmPass()"
					   placeholder="Подтверждение пароля" ng-model="passwords.new_confirm">
			</div>
		</div>

		<div class="checkboxes">
			
			<div class="col-md-12 col-xs-12 clearfix">
				<h3 class="def-title">Настройки уведомлений</h3>

                <? /* ?>

				<input type="checkbox" ng-true-value="'1'"
					   ng-false-value="'0'"
					   ng-model="userData.notifdz" id="test1" />
    			<label for="test1">Уведомление о новом ДЗ</label>

    			<input type="checkbox" ng-true-value="'1'"
					   ng-false-value="'0'"
					   ng-model="userData.notifmes" id="test2" />
    			<label for="test2">Уведомления о новом сообщении</label>
				<input type="checkbox" ng-true-value="'1'"
					   ng-false-value="'0'"
					    ng-model="userData.notifnews" id="test3" />
				<label for="test3">Новости и обновления сервиса</label>
				<input type="checkbox" ng-true-value="'1'"
					   ng-false-value="'0'"
					    ng-model="userData.notifend" id="test4" />
				<label for="test4">Уведомления об окончании действия тарифа</label>
				<input type="checkbox" ng-true-value="'1'"
					   ng-false-value="'0'"
					    ng-model="userData.notifreport" id="test5" />
				<label for="test5">Отчетное письмо по вашим курсам за день</label>

 <? */?>

                <div>
                   Уведомления будут о выполненных домашних заданиях и новых сообщениях в чате. (в разработке)
                </div>

                <input type="checkbox" ng-true-value="'1'"
                       disabled
                       ng-false-value="'0'"
                       ng-model="userData.vk_notif" id="test1" />
                <label for="test1">Получать уведомления VK</label>
                <div id="vk_send_message"></div>


                <input type="checkbox" ng-true-value="'1'"
                       ng-false-value="'0'"
                       ng-model="userData.mail_notif" id="test2" />
                <label for="test2">Получать уведомления на почту</label>



				<a href="" ng-click="saveSetting($event)" class="btn btn-access">Сохранить</a>
			</div>


			
			

		</div>

	</div>

</div>