<?

function lesson($lesson_id) {

    $username = chech_auth();
    $hash = $_COOKIE['hash'];
    $db = get_db();

    $id_user = $db->select('select id from users where hash={?}', array($hash));
    $id_user = $id_user[0]['id'];

    $cource_id = $db->select('SELECT cources.id FROM lessons join cources on lessons.cource_id = cources.id 
              WHERE lessons.id = {?}', array($lesson_id));
    $cource_id = $cource_id[0]['id'];


    $user_lesson = $db->select('SELECT id, send_files, stage FROM user_lessons WHERE id_user = {?} AND id_lesson = {?}',
        array($id_user, $lesson_id));


    access_cource($id_user,$cource_id);

    $result = $db->select('select * from lessons where id={?}',
        array($lesson_id));
    $name = $result[0]['name'];

    $cource = $db->select('SELECT * from cources where id = {?}',array($cource_id));

    $videos = $db->select('SELECT * from lesson_videos where id_lesson = {?} order by sort ASC',array($result[0]['id']));

    $ar_try = $db->select('SELECT homeworks.mark 
FROM homeworks 
JOIN user_lessons 
on homeworks.id_user_lesson = user_lessons.id
WHERE id_user_lesson = {?}
ORDER BY homeworks.try DESC',array($user_lesson[0]['id']));


    if (count($videos) > 0) {
        $result[0]['video'] = $videos;
    } else {
        $result[0]['video'] = '';
    }

    $messages = $db->select('select date_send, message, id_user_lesson, user_id, users.username 
from messages join users on messages.user_id = users.id WHERE id_user_lesson = {?} 
order by date_send ASC', array($user_lesson[0]['id']));

    for($i=0;$i<count($messages);$i++) {
        $messages[$i]['message'] = base64_decode($messages[$i]['message']);
    }

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => $name,
            'username' => $username,
            'chat_on' => true
        )
    );


        $all_count = $db->select('select count(id) as c from lessons where cource_id = {?}',array($cource_id));
        $all_count = $all_count[0]['c'];
        $complete_count = $db->select('Select COUNT(user_lessons.id) as c
                                        FROM user_lessons 
                                        JOIN lessons on user_lessons.id_lesson = lessons.id
                                        WHERE user_lessons.stage = "ACCESS" 
                                        AND user_lessons.id_user = {?} 
                                        AND lessons.cource_id = {?}', 
                                            array($id_user, $cource_id));
       

    $cource[0]['id_user'] = $id_user;
    $cource[0]['all_lesson'] = $all_count;
    $cource[0]['complete_lesson'] = $complete_count[0]['c'];

    echo loadView('templates/lesson.php',
        array(
            'lesson' => $result[0],
            'cource' => $cource[0],
            'send_files' => $user_lesson[0]['send_files'],
            'username' => $username,
            'user_lesson_id' =>$user_lesson[0]['id'],
            'messages' => $messages,
            'stage' => $user_lesson[0]['stage'],
            'ar_try' => $ar_try
        ));
    echo loadView('templates/footer.php',array());
}