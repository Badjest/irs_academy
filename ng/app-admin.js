var app = angular.module("app", ["ngCookies"]);


app.controller("PublicController", function($scope, $rootScope) {

    $scope.init = function (ROOT, BASE_LINK) {
        $rootScope.ROOT = ROOT;
        $rootScope.BASE_LINK = BASE_LINK;
    };

});


app.controller("chatAdmin", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.warning = 'Выберите пользователя';
    $scope.idchatroom = 0;
    $scope.lessons = [];
    $scope.id_user = 0;
    $scope.loading = false;
    $scope.username = '';
    $scope.message_wrapp = $('.message-wrapp');
    $scope.socket = io.connect('https://irs.academy:8091');

    //var wsUri = "wss://188.225.9.78:8091/demo/server.php";
    //var wsUri = "wss://irs.academy:8091/wss";
    //$scope.websocket = new WebSocket(wsUri);

    $scope.init = function(id_user, username)
    {
        $scope.id_user = +id_user;
        $scope.username  = username;
    };


    $scope.socket.on('connect', function(){
        $scope.socket.emit('adduser', $scope.username , 'Alone' , $scope.id_user);
    });

    $scope.socket.on('updatechat', function (username, message, date, id) {
         

            if (id === $scope.id_user) {
                    var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+username+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+date+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+message+'\
                        </div>\
                        </div>\
                        </div>';
                    $('#chatroom-'+$scope.idchatroom).append(messageTo);
                } else {
                    var messageFrom = '<div class="message-from clearfix">\
                    <div class="col-xs-5 "><div class="date">'+date+'</div></div>\
                    <div class="col-xs-7 "><span class="name_from">'+username+'</span></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-from">'+message+'\
                        </div>\
                        </div>\
                        </div>';
                    $('#chatroom-'+$scope.idchatroom).append(messageFrom);
                }
                $scope.message_wrapp.scrollTop($scope.message_wrapp.prop("scrollHeight"));
    });

   

    

    $scope.loadCources = function (id_user) {
        $scope.warning = '';
        $scope.message_wrapp.html('');
        if (!$scope.loading) {
            $.ajax({
                method: "POST",
                url: $rootScope.ROOT + "/getChatCources",
                data: {'id_user': id_user},
                beforeSend: function () {
                    $scope.loader.css('opacity', '1');
                    $scope.loader.css('width', '100%');
                    $scope.loading = true;
                }
            }).success(function (data) {
                data = JSON.parse(data);
                $scope.loading = false;
                $scope.loader.css('width', '0%');
                $scope.loader.css('opacity', '0');
                $scope.lessons = data;
                if ($scope.lessons.length === 0) {
                    $scope.warning = 'У пользователя нет активных уроков';
                }
                $scope.$apply();
            });
        }
    };

    $scope.loadMessages = function (chatroom, id_user, current_item) {
        $scope.idchatroom = chatroom;
        $scope.current_user_id = id_user;
        $scope.current_item = current_item;
        $scope.message_wrapp.html('<div id="chatroom-'+chatroom+'"></div>');
        $.ajax({
            method: "POST",
            url: $rootScope.ROOT + "/getChatCources/"+chatroom,
            beforeSend: function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
            }
        }).success(function (data) {
            
            $scope.socket.emit('switchRoom', $scope.idchatroom);
            

            var q_read = $scope.lessons[$scope.lessons.indexOf($scope.current_item)].no_read;
            var common_noread = $('.no-read-'+$scope.current_user_id);
            var common_noread_q = 0;
            if (common_noread.length > 0) {
                common_noread_q = +common_noread.html();

                if ((common_noread_q - q_read) <= 0) {
                    common_noread.remove();
                } else {
                    common_noread.html(common_noread_q - q_read);
                }
            }

            $scope.lessons[$scope.lessons.indexOf($scope.current_item)].no_read = 0;
            $scope.$apply();


            data = JSON.parse(data);
            $scope.loader.css('width', '0%');
            $scope.loader.css('opacity', '0');

            if (data.length === 0) {
                var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr"></div>\
                    <div class="col-xs-5 npl"><div class="date"></div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">Диалогов нет\
                        </div>\
                        </div>\
                        </div>';
                $('#chatroom-'+$scope.idchatroom).append(messageTo);
            } else {

                for (var i = 0; i < data.length; i++ ) {
                    if (data[i].my) {
                        var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+data[i].username+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+data[i].date_send+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+data[i].message+'\
                        </div>\
                        </div>\
                        </div>';
                        $('#chatroom-'+$scope.idchatroom).append(messageTo);
                    } else {
                        var messageFrom = '<div class="message-from clearfix">\
                    <div class="col-xs-5 "><div class="date">'+data[i].date_send+'</div></div>\
                    <div class="col-xs-7 "><span class="name_from">'+data[i].username+'</span></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-from">'+data[i].message+'\
                        </div>\
                        </div>\
                        </div>';
                        $('#chatroom-'+$scope.idchatroom).append(messageFrom);
                    }
                }

            }

        });
    };

    $scope.sendMessage = function (e, idchatroom) {
        e.preventDefault();
        var fieldSend = $('.textTo');
        var message = fieldSend.val();
        message = message.replace(/\n/g, '<br>');
        $scope.socket.emit('sendchat', message);
        fieldSend.val('');
    };



});


app.controller("HeaderController", function($scope, $cookies, $rootScope) {
    $scope.loader = $('.loader .line');

    $scope.exitUser = function(e) {
        e.preventDefault();
        $cookies.remove("hash", { path: $rootScope.ROOT });
        $scope.loader.css('width', '100%');
        window.location.href = $rootScope.ROOT + '/auth';
    }
});


app.controller("AdminUserId", function($scope , $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.userData = {};
    $scope.cources = [];

    $scope.init = function (source, cources) {
        $scope.userData = source;
        $scope.cources = cources;

        for (var i = 0; i < $scope.cources.length; i++) {
            for (var j = 0; j < $scope.cources[i].lessons.length; j++) {
                $scope.cources[i].lessons[j].sort = parseInt($scope.cources[i].lessons[j].sort);
            }
        }
    };

    $scope.blockUser = function (state) {
        $scope.userData.block = state;
        $.ajax({
            method: "POST",
            url: $rootScope.ROOT + "/saveUserPayAndSetting",
            data: { 'userSetting': $scope.userData , 'courses': $scope.cources },
            beforeSend: function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
                $scope.loading = true;
            }
        }).success(function (data) {
            data = JSON.parse(data);
            if (data.success) {
                success('Сохранено');
            } else {
                error(data.message);
            }
            $scope.loader.css('opacity', '0');
            $scope.loader.css('width', '0%');
        });
    };


    $scope.openAllLessons = function(id_lesson) {
        console.log(id_lesson);
        console.log($scope.userData);
        $.ajax({
            method: "POST",
            url: $rootScope.ROOT + "/openAllLessonsForUser",
            data: { 
                'cource_id': id_lesson , 
                'id_user': $scope.userData.id 
            },
            beforeSend: function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
                $scope.loading = true;
            }
        }).success(function (data) {
            data = JSON.parse(data);
            if (data.success) {
                window.location.reload();
            } else {
                error(data.message);
            }
            $scope.loader.css('opacity', '0');
            $scope.loader.css('width', '0%');
        });
    };


    $scope.saveSet = function (e) {
        e.preventDefault();

        $.ajax({
            method: "POST",
            url: $rootScope.ROOT + "/saveUserPayAndSetting",
            data: { 'userSetting': $scope.userData , 'courses': $scope.cources },
            beforeSend: function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
                $scope.loading = true;
            }
        }).success(function (data) {
            data = JSON.parse(data);
            if (data.success) {
                success('Сохранено');
            } else {
                error(data.message);
            }
            $scope.loader.css('opacity', '0');
            $scope.loader.css('width', '0%');
        });
    };

});

app.controller("AdminWebinars", function($scope, $rootScope) {

    $scope.webinars = [];

    $scope.init = function (webinars) {
        $scope.webinars = webinars;
    };

    $scope.newWebinar = function () {
        $scope.webinars.push({
            name:'',
            url: '',
            del: true
        });
    };

    $scope.saveWebinars = function () {
        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT +'/saveWebinars/',
            data : { webinars : $scope.webinars }
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
               window.location.reload();
            } else {
                error(result.message);
            }
        });
    };

    $scope.deleteWebinar = function (index) {
        if ($scope.webinars[index].del) {
            $scope.webinars.splice(index, 1);
            $scope.$apply();
            return false;
        }

        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT +'/delWebinar/'+ $scope.webinars[index].id
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                success(result.message);
                $scope.webinars.splice(index, 1);
                $scope.$apply();
            } else {
                error(result.message);
            }
        });
    };

});

app.controller("webinarModer", function($scope, $rootScope) {

    $scope.url = '';
    $scope.messages = [];

    $scope.initSocket = function (url, messages) {
        $scope.socket = io.connect('https://irs.academy:8098');

        $scope.url = url;
        $scope.messages = messages;

        $scope.socket.on('connect', function(){
            $scope.socket.emit('adduser', 'Модератор' , url);
        });

        $scope.socket.on('updatechat', function (username, message, date) {

            var messageField = $('.message-wrapp.chatModer');

            var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+username+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+date+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+message+'\
                        </div>\
                        </div>\
                        </div>';
            messageField.append(messageTo);

            messageField.scrollTop(messageField.prop("scrollHeight"));

        });


        $scope.socket.on('updatechatPre', function (username, message, date) {

            var messageField = $('.message-wrapp.preChat');

            $scope.messages.unshift({
                message : message,
                name :  username,
                date : date
            });

            $scope.$apply();

            // var messageTo = '<div class="message-to clearfix">\
            //             <div class="col-xs-7 npr">'+username+'</div>\
            //         <div class="col-xs-5 npl"><div class="date">'+date+'</div></div>\
            //         <div class="col-xs-12">\
            //             <div class="message-text message-text-to">'+message+'\
            //             <button ng-click="sendMessageAcceptUser($event, \''+username+'\' , \''+message+'\' )" >Одобрить</button>\
            //             </div>\
            //             </div>\
            //             </div>';
            // messageField.prepend(messageTo);

        });

        $scope.sendMessageModer = function (e) {
            e.preventDefault();

            var fieldSend = $('.textTo.Moder');
            var message = fieldSend.val();
            $scope.socket.emit('sendchat', message);
            fieldSend.val('');
        };

        $scope.sendMessageAcceptUser = function (e, user, message, hash) {
            e.preventDefault();
            $scope.socket.emit('sendchatfrommoder', $scope.url, user ,message , hash);
        };

    }


});

function error(message)
{
    noty({
        text: message ,
        timeout: 5000,
        type:'error',
        layout: 'topRight'
    });
}

function success(message)
{
    noty({
        text: message,
        timeout: 5000,
        type:'success',
        layout: 'topRight'
    });
}
