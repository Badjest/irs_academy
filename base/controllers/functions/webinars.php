<?

function webinars() {

    if (is_admin()) {

        $db = get_db();
        $webinars = $db->select('Select * from webinars');
        $username = chech_auth();

        echo loadView('templates/header.php',
            array(
                'show_header'=>true,
                'title' => 'Вебинары',
                'username' => $username
            )
        );
        echo loadView('templates/webinars.php',array(
            'webinars'=> $webinars
        ));
        echo loadView('templates/footer.php',array(
            'admin_ng' => true
        ));
    }


}