<?php
$basic = include "base/config/basic_var.php";

define("ROOT",$basic->root);
define("BASE_LINK",$basic->base_link);

require_once 'classes/VK/VK.php';
require_once 'classes/VK/VKException.php';
require_once 'classes/RouterLite/RouterLite.php';
require_once 'controllers/index.php';

global $vk;
$vk = new VK\VK('{6327856}', '{ZmNBwbEx7b9V13tlY6bd}');



RouterLite::addRoute(ROOT, 'check_auth');
RouterLite::addRoute(ROOT.'/forget', 'forget_pass');
RouterLite::addRoute(ROOT.'/auth', 'check_auth');
RouterLite::addRoute(ROOT.'/my', 'my');
RouterLite::addRoute(ROOT.'/my/:num', 'lessonsOn/$1');
RouterLite::addRoute(ROOT.'/lesson/:num', 'lesson/$1');
RouterLite::addRoute(ROOT.'/settings', 'settings');
RouterLite::addRoute(ROOT.'/admin_users', 'admin_users');
RouterLite::addRoute(ROOT.'/admin_user/:num', 'admin_user_id/$1');
RouterLite::addRoute(ROOT.'/admin_cource', 'admin_cource');
RouterLite::addRoute(ROOT.'/admin_cource/new', 'admin_cource_new');
RouterLite::addRoute(ROOT.'/admin_cources/edit/:num', 'admin_cource_edit/$1');
RouterLite::addRoute(ROOT.'/lesson/edit/:num', 'lesson_edit/$1');
RouterLite::addRoute(ROOT.'/homeworks', 'homeworks');
RouterLite::addRoute(ROOT.'/messages', 'messages');
RouterLite::addRoute(ROOT.'/homework/:num', 'homework/$1');
RouterLite::addRoute(ROOT.'/julia_teach_me', 'julia_teach_me');
RouterLite::addRoute(ROOT.'/webinar/:any', 'webinar/$1');
RouterLite::addRoute(ROOT.'/webinarmoder/:any', 'webinarModer/$1');
RouterLite::addRoute(ROOT.'/webinars', 'webinars');
RouterLite::addRoute(ROOT.'/scores', 'scores');
RouterLite::addRoute(ROOT.'/test', 'test');



//API
RouterLite::addRoute(ROOT.'/login', 'login');
RouterLite::addRoute(ROOT.'/saveUser', 'saveUser');
RouterLite::addRoute(ROOT.'/saveUsers', 'saveUsers');
RouterLite::addRoute(ROOT.'/recoverPass', 'recoverPass');
RouterLite::addRoute(ROOT.'/sendData', 'sendData');
RouterLite::addRoute(ROOT.'/saveCource', 'saveCource');
RouterLite::addRoute(ROOT.'/saveLesson', 'saveLesson');
RouterLite::addRoute(ROOT.'/delCource', 'deleteCource');
RouterLite::addRoute(ROOT.'/openAllLessonsForUser', 'openAllLessonsForUser');
RouterLite::addRoute(ROOT.'/delLesson/:num', 'deleteLesson/$1');
RouterLite::addRoute(ROOT.'/getChatCources', 'getChatCources');
RouterLite::addRoute(ROOT.'/getChatCources/:num', 'getChatMessages/$1');
RouterLite::addRoute(ROOT.'/saveStatusLesson', 'saveStatusLesson');
RouterLite::addRoute(ROOT.'/setMarkToTry', 'setMarkToTry');
RouterLite::addRoute(ROOT.'/saveUserPayAndSetting', 'saveUserPayAndSetting');
RouterLite::addRoute(ROOT.'/changeStatusHomeworkRead/:num', 'changeStatusHomeworkRead/$1');
RouterLite::addRoute(ROOT.'/uploadFile', 'uploadFile');
RouterLite::addRoute(ROOT.'/delWebinars/:num', 'delWebinars/$1');
RouterLite::addRoute(ROOT.'/saveWebinars', 'saveWebinars');
RouterLite::addRoute(ROOT.'/delWebinarMessage/:num', 'delWebinarMessage/$1');
RouterLite::addRoute(ROOT.'/saveWebinar', 'saveWebinar');
RouterLite::addRoute(ROOT.'/sendWebinarModal', 'sendWebinarModal');
RouterLite::addRoute(ROOT.'/checkNotif', 'checkNotif');
RouterLite::addRoute(ROOT.'/saveCheckForm', 'saveCheckForm');
RouterLite::addRoute(ROOT.'/getDateNow', 'getDateNow');
RouterLite::addRoute(ROOT.'/saveWebinarMessage', 'saveFakeMessage');


//RouterLite::addRoute(ROOT.'/chsl', 'chSl');

RouterLite::dispatch();
