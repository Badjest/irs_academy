var mysql = require('mysql');
var http = require('http');
var express = require('express'),
    app = module.exports.app = express();
var moment = require('moment');
var schedule = require('node-schedule');

//env TZ='Asia/Novosibirsk' node app-fake.js

var server = http.createServer(app);
var io = require('socket.io').listen(server);  //pass a http.Server instance
server.listen(8099);

var pool = mysql.createPool({
    host     : 'localhost',
    port 	   : '/var/run/mysqld/mysqld.sock', //port: '/Applications/MAMP/tmp/mysql/mysql.sock'
    user     : 'irsibnsk_newdb',
    password : 'BX9Oi5C;-C',
    database : 'irsibnsk_newdb'
});

io.sockets.on('connection', function (socket) {

	socket.on('adduser', function(username, idroom){

		socket.username = username;

		socket.room = 'room' + idroom;

		socket.join('room' + idroom);

		socket.roomPre = 'roomPre' + idroom;

        socket.join('roomPre' + idroom);

	});

	socket.on('sendchat', function (message) {

		var datenow = moment(new Date()).format("HH:mm:ss");
		io.sockets.in(socket.room).emit('updatechat', socket.username, message, datenow);

	});

    socket.on('preModer', function (username , message , hash) {

        var datenow = moment(new Date()).format("HH:mm:ss");
        io.sockets.in(socket.roomPre).emit('updatechatPre', username , message, datenow , hash);

    });

    socket.on('sendchatfrommoder', function (room,user,message, hash) {
        var datenow = moment(new Date()).format("HH:mm:ss");
        io.sockets.in('room' + room).emit('updatechat', user, message, datenow, hash);
    });

    socket.on('openAllButtons', function () {
        io.sockets.in(socket.room).emit('openButtons');
    });

    socket.on('closeAllButtons', function () {
        io.sockets.in(socket.room).emit('closeButtons');
    });


	socket.on('disconnect', function() {
		socket.leave('room' +socket.room);
		socket.leave('roomPre' +socket.roomPre);
	});
});

app.get('/', function (req, res) {});

app.get('/sendMessage', function (req, res) {

	var id = req.query.id;
	var room = req.query.room;

	pool.getConnection(function(err,connection){
        if (err) {
            console.log('Ошибка соединения:'+ err);
            return;
        }

        connection.query("SELECT * from webinars_message where id = " + id ,
			function(error, results, fields){
            connection.release();
            if(!err) {

            	var datenow = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
                io.sockets.in('room'+room)
					.emit('updatechat', results[0].name , results[0].message, datenow);

                //callback(true);
            } else {
                console.log('Ошибка добавления:' + err);
            }
        });
        connection.on('error', function(err) {
            console.log(err);
            return;
        });
    });

    res.send('ok');
});


var j = 0;

// установка задач по времени для сообщений
app.post('/setNewTime', function (req, res) {

    pool.getConnection(function(err,connection){
        if (err) {
            console.log('Ошибка соединения:'+ err);
            return;
        }

        var jobList = schedule.scheduledJobs;

        var l = 0;

        for(jobName in jobList) {
            var job = 'jobList.Announcement'+l;

			eval(job+'.cancel()');
			l++;
        }

        j = 0;

        connection.query("SELECT webinars_message.message, webinars_message.time, webinars_message.name , webinars.url FROM webinars_message JOIN webinars on webinars_message.id_webinar = webinars.id" ,
            function(error, results, fields){
                connection.release();
                if(!err) {

                	if (results != undefined) {
                        for (var i = 0; i < results.length; i++) {
                            var hour = +results[i].time.split(':')[0];
                            var minute = +results[i].time.split(':')[1];
                            var second = +results[i].time.split(':')[2];

                            schedule.scheduleJob('Announcement' + i ,{hour: hour, minute: minute , second: second}, function(record){
                                var datenow = moment(new Date()).format("HH:mm:ss");


                                io.sockets.in('room'+record.url)
                                    .emit('updatechat', record.name , record.message, datenow);

                            }.bind(null, results[i]));
                            j++;

                        }
					}

					//console.log(schedule.scheduledJobs);

                } else {
                    console.log('Ошибка:' + err);
                }
            });
        connection.on('error', function(err) {
            console.log(err);
            return;
        });

    });


    res.send('ok');
});

app.post('/setNewTimeOpen', function (req, res) {

    pool.getConnection(function(err,connection){
        if (err) {
            console.log('Ошибка соединения:'+ err);
            return;
        }


        connection.query( "SELECT * FROM webinars" ,
            function(error, results, fields){
                connection.release();
                if(!err) {

                    if (results != undefined) {

                        j--;

                        for (var i = 0; i < results.length; i++) {
                            var hour = +results[i].time_open_but.split(':')[0];
                            var minute = +results[i].time_open_but.split(':')[1];
                            var second = +results[i].time_open_but.split(':')[2];

                            j++;

                            schedule.scheduleJob('Announcement' + j ,
                                {hour: hour, minute: minute , second: second}, function(record) {

                                io.sockets.in('room'+record.url).emit('openButtons');

                            }.bind(null, results[i]));

                            hour = +results[i].timer_close.split(':')[0];
                            minute = +results[i].timer_close.split(':')[1];
                            second = +results[i].timer_close.split(':')[2];


                            schedule.scheduleJob('Announcement' + j ,
                                {hour: hour, minute: minute , second: second}, function(record) {

                                    io.sockets.in('room'+record.url).emit('closeButtons');

                                }.bind(null, results[i]));


                            j++;

                            hour = +results[i].start_video.split(':')[0];
                            minute = +results[i].start_video.split(':')[1];
                            second = +results[i].start_video.split(':')[2];

                            schedule.scheduleJob('Announcement' + j ,
                                {hour: hour, minute: minute , second: second}, function(record) {

                                    io.sockets.in('room'+record.url).emit('openVideo');

                                }.bind(null, results[i]));

                            j++;

                            hour = +results[i].end_video.split(':')[0];
                            minute = +results[i].end_video.split(':')[1];
                            second = +results[i].end_video.split(':')[2];

                            schedule.scheduleJob('Announcement' + j ,
                                {hour: hour, minute: minute , second: second}, function(record) {

                                    io.sockets.in('room'+record.url).emit('closeVideo');

                                }.bind(null, results[i]));

                        }

                    }


                   // console.log(schedule.scheduledJobs);


                } else {
                    console.log('Ошибка:' + err);
                }
            });
        connection.on('error', function(err) {
            console.log(err);
            return;
        });

    });


    res.send('ok');
});


console.log('Сервер запущен!Ты опущен!');