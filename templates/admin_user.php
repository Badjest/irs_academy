<? $data = $arrayOfData;?>
<div class="container" ng-controller="adminUser" ng-init="init(<?php echo htmlspecialchars(json_encode($data['users'])); ?>,
'<?=ROOT?>')">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		

		
		<h3 class="def-title">Пользователи</h3>

        <form method="post" action="">
            <input type="text" name="word" value="<?=$_POST['word']?>" placeholder="имя или фамилия или email">
        </form>

		<a href="" ng-click="addUser($event)"  class="btn fly">Добавить <br> пользователя</a>
		<a href="" ng-click="saveSet($event)" class="btn fly-save">Сохранить <br> изменения</a>

        <div>
            <? if (count($_GET) > 0) { ?>
                <a href="<?=$_SERVER['REQUEST_URI']?>&orderby=last_use">Сортировать по дате посещений</a>
            <? } else { ?>
            <a href="?orderby=last_use">Сортировать по дате посещений</a>
            <? } ?>
        </div>
			

		<div class="my-courses">
			
			<div ng-repeat="item in users track by $index" class="course-item clearfix">
				
				<div class="col-md-3 col-xs-6">
					<label for="name">Имя</label>
					<input type="text" ng-model="item.username" value="{{item.username}}" id="name">
				</div>
				<div class="col-md-3 col-xs-6">
					<label for="mail">Почта</label>
					<input type="text" ng-model="item.mail" value="{{item.mail}}" id="mail">
				</div>
				<div class="col-md-3 col-xs-6">
					<label for="pass">Пароль</label>
					<input type="text" ng-model="item.pass" value="{{item.pass}}" id="pass">
				</div>
				<div class="col-md-3 col-xs-6">
					<label for="phone">Телефон</label>
					<input type="text" ng-model="item.phone" value="{{item.phone}}" id="phone">
				</div>
				<div class="col-md-3 col-xs-6">
					<label for="fam">Фамилия</label>
					<input type="text" ng-model="item.userlastname" value="{{item.userlastname}}" id="fam">
				</div>

				<div class="col-md-3 col-xs-6">
					<input style="margin-top: 15px;" type="checkbox" ng-true-value="'1'"
						   ng-false-value="'0'"
						   ng-model="item.admin" id="admin-{{$index}}" />
					<label style="margin-top: 30px;" for="admin-{{$index}}">Админ</label>
				</div>

                <div class="col-md-3 col-xs-6">
                    Последний вход: {{ item.last_use }}
                </div>

				<div class="col-md-3 col-xs-6">
					<a ng-if="item.id" class="btn btn-access" href="{{ROOT}}/admin_user/{{item.id}}">Редактировать</a>
				</div>


			</div>

		</div>


        <? if ($data['count_pages'] > 1) { ?>
        
            <h2>Cтраницы:</h2>
            <div class="pagin">
                <? for($i = 1; $i < ($data['count_pages'] + 1); $i++ ) { ?>
                    <a href="?page=<?=$i?>"><?=$i?></a>
                <? } ?>
            </div>
        
        <? } ?>

	</div>

</div>