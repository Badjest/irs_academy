<?

function lessonsOn($cource_id) {


    $username = chech_auth();

    $hash = $_COOKIE['hash'];
    $db = get_db();
    $id_user = $db->select('select id from users where hash={?}', array($hash));
    $id_user = $id_user[0]['id'];
    access_cource($id_user,$cource_id);
    $name = $db->select('Select name from cources where id = {?}',array($cource_id));
    $result = $db->select('select id, name, cource_id, timefor, photo from lessons where cource_id={?} ORDER BY sort ASC',
        array($cource_id));

    for ($i=0;$i < count($result); $i++) {
        $select = $db->select('SELECT stage , pay FROM user_lessons WHERE id_user = {?} AND id_lesson = {?}',
            array($id_user, $result[$i]['id']));
        if (count($select) > 0 ) {
            $result[$i]['stage'] = $select[0]['stage'];

            if ($select[0]['pay'] === '0') {
                $result[$i]['name'] = 'Оплатите остаток курса, чтобы продолжить. Для оплаты свяжитесь с преподавателем в VK:
                <a href="https://vk.com/julia_teach_me">Юлия</a>';
            }

        } else {
            $db->query('insert into user_lessons set id_lesson = {?}, id_user={?}, stage="CLOSE" , pay = 0',
                array($result[$i]['id'], $id_user));
            $result[$i]['stage'] = 'CLOSE';
            $result[$i]['name'] = 'Оплатите остаток курса, чтобы продолжить. Для оплаты свяжитесь с преподователем в VK:
                <a href="https://vk.com/julia_teach_me">Юлия</a>';
        }

    }

    $cource = $db->select('SELECT * from cources where id = {?}',array($cource_id));

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => $name[0]['name'],
            'username' => $username
        )
    );
    echo loadView('templates/lessons.php',
        array(
            'lessons' => $result,
            'cource' => $cource[0]
        ));
    echo loadView('templates/footer.php',array());
}