<?php

date_default_timezone_set('Asia/Novosibirsk');

$db = Db::getDB(
    (object) array(
        'host' => 'localhost',
        'user' => 'irsibnsk_newdb',
        'password' => 'BX9Oi5C;-C',
        'dbname' => 'irsibnsk_newdb'
    ));

$accessTokens = array(
    0 => '5a99b1870c12f9f8fffd63e38927fe1bd3e9657807f7458bb12d681f918085d2abaf12a3c336cb8174230',
    1 => '365be758cad8b080c9f4c05c2c6c70f6b0f004019078c4e2c300596c3bf802b56d1b0603aa9045056aa9e',
    2 => '6687f4703824b123addbfe9f19306195770a841e44b35c13be34ba7bd20f303c29c16ebfbc4117f7966be'
);

$vk = new VK('{6189467}', '{jDQ82PPRfIU9yLP7ztHj}');


$message = 'Добрый день! Скоро новый год, а это значит, впереди новые цели и достижения. 

Специально для тех, кто ждал удобного момента, чтобы начать новую жизнь в новом году, у меня есть спeциaльное прeдложение: 2 обучающие программы с новогодней скидкoй 75% от стоимости. 

Для Вас, до 18.12.2017, цена за два курса составит всего 5700 рублей! 

Курс по ретуши 10 уроков + ДЗ + обратная связь преподавателя 9900 рублей 
Курс "Основы фотографии" - 10 уроков + ДЗ + обратная связь преподавателя - 12900 рублей 

Не упусти возможность! Напиши мне, расскажу подробнее😽';


$users = $db->select('select * from new_year_clients where send = 0 LIMIT 2');

foreach ($users as $user) {

    $db->query('update new_year_clients set send = 3 where id = {?}', array($user['id']));

    foreach ($accessTokens as $token) {

        $vk->setAccessToken($token);

        $is_friend = $vk->api('friends.areFriends', array(
            'user_ids'   =>  $user['id_vk']
        ));

        if ($is_friend['response'][0]['friend_status'] === 3) {

            $send_message = $vk->api('messages.send', array(
                'user_ids' => $user['id_vk'],
                'message' => $message
            ));

            if ($send_message['error']) {
                $db->query('update new_year_clients set send = 0 where id = {?}', array($user['id']));
                $file = __DIR__ . '/log.txt';

                $current = file_get_contents($file);

                $current .= "Для токена ".$token." нет доступа\n";

                file_put_contents($file, $current);
            } else {
                $db->query('update new_year_clients set send = 1 where id = {?}', array($user['id']));
            }

            break;

        }

    }

}

$file = __DIR__ . '/log.txt';

$current = file_get_contents($file);

$date = new DateTime();

$date->setTimezone(new DateTimeZone('Asia/Novosibirsk'));
$date_now = $date->format("H:i:s Y-m-d");

$current .= $date_now .  " Скрипт выполнился\n";

file_put_contents($file, $current);





class Db {

    private static $db = null;
    private $mysqli;
    private $sym_query = "{?}";

    public static function getDB($config) {
        if (self::$db == null) self::$db = new Db($config);
        return self::$db;
    }

    private function __construct($config) {
        $this->mysqli = new mysqli($config->host ,$config->user , $config->password, $config->dbname);
        $this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    private function getQuery($query, $params) {
        if ($params) {
            for ($i = 0; $i < count($params); $i++) {
                $pos = strpos($query, $this->sym_query);
                $arg = "'".$this->mysqli->real_escape_string($params[$i])."'";
                $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
            }
        }
        return $query;
    }

    public function select($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if (!$result_set) return  $this->mysqli->error;
        return $this->resultSetToArray($result_set);
    }

    public function query($query, $params = false) {
        $success = $this->mysqli->query($this->getQuery($query, $params));
        if ($success) {
            if ($this->mysqli->insert_id === 0) return true;
            else return $this->mysqli->insert_id;
        }
        else return $this->mysqli->error;
    }

    private function resultSetToArray($result_set) {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = $row;
        }
        return $array;
    }

    public function __destruct() {
        if ($this->mysqli) $this->mysqli->close();
    }

}


class VK
{
    /**
     * VK application id.
     * @var string
     */
    private $app_id;
    /**
     * VK application secret key.
     * @var string
     */
    private $api_secret;
    /**
     * API version. If null uses latest version.
     * @var int
     */
    private $api_version;
    /**
     * VK access token.
     * @var string
     */
    private $access_token;
    /**
     * Authorization status.
     * @var bool
     */
    private $auth = false;
    /**
     * Instance curl.
     * @var Resource
     */
    private $ch;
    const AUTHORIZE_URL = 'https://oauth.vk.com/authorize';
    const ACCESS_TOKEN_URL = 'https://oauth.vk.com/access_token';
    /**
     * Constructor.
     * @param   string $app_id
     * @param   string $api_secret
     * @param   string $access_token
     * @throws  VKException
     */
    public function __construct($app_id, $api_secret, $access_token = null)
    {
        $this->app_id = $app_id;
        $this->api_secret = $api_secret;
        $this->setAccessToken($access_token);
        $this->ch = curl_init();
    }
    /**
     * Destructor.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }
    /**
     * Set special API version.
     * @param   int $version
     * @return  void
     */
    public function setApiVersion($version)
    {
        $this->api_version = $version;
    }
    /**
     * Set Access Token.
     * @param   string $access_token
     * @throws  VKException
     * @return  void
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }
    /**
     * Returns base API url.
     * @param   string $method
     * @param   string $response_format
     * @return  string
     */
    public function getApiUrl($method, $response_format = 'json')
    {
        return 'https://api.vk.com/method/' . $method . '.' . $response_format;
    }
    /**
     * Returns authorization link with passed parameters.
     * @param   string $api_settings
     * @param   string $callback_url
     * @param   bool $test_mode
     * @return  string
     */
    public function getAuthorizeUrl($api_settings = '',
                                    $callback_url = 'https://api.vk.com/blank.html', $test_mode = false)
    {
        $parameters = array(
            'client_id' => $this->app_id,
            'scope' => $api_settings,
            'redirect_uri' => $callback_url,
            'response_type' => 'code'
        );
        if ($test_mode)
            $parameters['test_mode'] = 1;
        return $this->createUrl(self::AUTHORIZE_URL, $parameters);
    }
    /**
     * Returns access token by code received on authorization link.
     * @param   string $code
     * @param   string $callback_url
     * @throws  VKException
     * @return  array
     */
    public function getAccessToken($code, $callback_url = 'https://api.vk.com/blank.html')
    {
        if (!is_null($this->access_token) && $this->auth) {
            throw new VKException('Already authorized.');
        }
        $parameters = array(
            'client_id' => $this->app_id,
            'client_secret' => $this->api_secret,
            'code' => $code,
            'redirect_uri' => $callback_url
        );
        $rs = json_decode($this->request(
            $this->createUrl(self::ACCESS_TOKEN_URL, $parameters)), true);
        if (isset($rs['error'])) {
            throw new VKException($rs['error'] .
                (!isset($rs['error_description']) ?: ': ' . $rs['error_description']));
        } else {
            $this->auth = true;
            $this->access_token = $rs['access_token'];
            return $rs;
        }
    }
    /**
     * Return user authorization status.
     * @return  bool
     */
    public function isAuth()
    {
        return !is_null($this->access_token);
    }
    /**
     * Check for validity access token.
     * @param   string $access_token
     * @return  bool
     */
    public function checkAccessToken($access_token = null)
    {
        $token = is_null($access_token) ? $this->access_token : $access_token;
        if (is_null($token)) return false;
        $rs = $this->api('getUserSettings', array('access_token' => $token));
        return isset($rs['response']);
    }
    /**
     * Execute API method with parameters and return result.
     * @param   string $method
     * @param   array $parameters
     * @param   string $format
     * @param   string $requestMethod
     * @return  mixed
     */
    public function api($method, $parameters = array(), $format = 'array', $requestMethod = 'get')
    {
        $parameters['timestamp'] = time();
        $parameters['api_id'] = $this->app_id;
        $parameters['random'] = rand(0, 10000);
        if (!array_key_exists('access_token', $parameters) && !is_null($this->access_token)) {
            $parameters['access_token'] = $this->access_token;
        }
        if (!array_key_exists('v', $parameters) && !is_null($this->api_version)) {
            $parameters['v'] = $this->api_version;
        }
        ksort($parameters);
        $sig = '';
        foreach ($parameters as $key => $value) {
            $sig .= $key . '=' . $value;
        }
        $sig .= $this->api_secret;
        $parameters['sig'] = md5($sig);
        if ($method == 'execute' || $requestMethod == 'post') {
            $rs = $this->request(
                $this->getApiUrl($method, $format == 'array' ? 'json' : $format), "POST", $parameters);
        } else {
            $rs = $this->request($this->createUrl(
                $this->getApiUrl($method, $format == 'array' ? 'json' : $format), $parameters));
        }
        return $format == 'array' ? json_decode($rs, true) : $rs;
    }
    /**
     * Concatenate keys and values to url format and return url.
     * @param   string $url
     * @param   array $parameters
     * @return  string
     */
    private function createUrl($url, $parameters)
    {
        $url .= '?' . http_build_query($parameters);
        return $url;
    }
    /**
     * Executes request on link.
     * @param   string $url
     * @param   string $method
     * @param   array $postfields
     * @return  string
     */
    private function request($url, $method = 'GET', $postfields = array())
    {
        curl_setopt_array($this->ch, array(
            CURLOPT_USERAGENT => 'VK/1.0 (+https://github.com/vladkens/VK))',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST => ($method == 'POST'),
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_URL => $url
        ));
        return curl_exec($this->ch);
    }
}
;


class VKException extends \Exception {  }


//$handle = fopen("friends.txt", "r");
//if ($handle) {
//
//    while (($line = fgets($handle)) !== false) {
//        $db->query('insert into new_year_clients set id_vk = {?}', array($line));
//    }
//
//    fclose($handle);
//} else {
//    echo 'не удалось прочитать';
//}



//$start = 0;
//$resultDeals = array();
//
//// выбираем все сделки со стадией
//do {
//
//    $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.list.json?start=' . $start;
//
//    $queryData = http_build_query(array(
//        'order' =>  array(),
//        'filter' => array(
//            "CATEGORY_ID" => 14,
//            'STAGE_ID' => array('C14:WON' , 'C14:FINAL_INVOICE')
//        ),
//        'select' => array("ID", "TITLE", "STAGE_ID", "CONTACT_ID"),
//    ));
//
//    $curl = curl_init();
//    curl_setopt_array($curl, array(
//        CURLOPT_SSL_VERIFYPEER => 0,
//        CURLOPT_POST => 1,
//        CURLOPT_HEADER => 0,
//        CURLOPT_RETURNTRANSFER => 1,
//        CURLOPT_URL => $queryUrl,
//        CURLOPT_POSTFIELDS => $queryData,
//    ));
//
//    $result = curl_exec($curl);
//
//    curl_close($curl);
//    $result = json_decode($result, 1);
//    $start = $result['next'];
//
//    $resultDeals = array_merge($resultDeals, $result['result']);
//
//    //var_dump($resultDeals);
//
//} while ($result['next']);
//
//$ids = array();
//
//// формируем массив с id контактов
//foreach ($resultDeals as $value) {
//
//        $ids[] = $value['CONTACT_ID'];
//}
//
//
//
//
//$resultContacts = array();
//
//
//if (count($ids) > 0) {
//
//    $start = 0;
//
//
//// выбираем всех контактов с совпавшими id
//    do {
//
//
//        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.contact.list.json?start=' . $start;
//
//        $queryData = http_build_query(array(
//            'order' =>  array(),
//            'filter' => array( 'ID' =>  $ids),
//            'select' => array("ID", "NAME", "WEB" , "ASSIGNED_BY_ID")
//        ));
//
//
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_SSL_VERIFYPEER => 0,
//            CURLOPT_POST => 1,
//            CURLOPT_HEADER => 0,
//            CURLOPT_RETURNTRANSFER => 1,
//            CURLOPT_URL => $queryUrl,
//            CURLOPT_POSTFIELDS => $queryData,
//        ));
//
//        $web = curl_exec($curl);
//        curl_close($curl);
//        $web = json_decode($web, 1);
//
//        $start = $web['next'];
//
//        $resultContacts = array_merge($resultContacts, $web['result']);
//
//    } while ($web['next']);
//
//}
//
//
//foreach ($resultContacts as $item) {
//    if ($item["WEB"]) {
//        $pos = strrpos($item['WEB'][0]['VALUE'], "id");
//        $find_and_delete = '';
//        if ($pos === false) {
//            $find_and_delete = $item['WEB'][0]['VALUE'];
//        } else {
//            $find_and_delete = substr($item['WEB'][0]['VALUE'], 2);
//        }
//        //var_dump($find_and_delete);
//        $db->query('delete from new_year_clients where LOCATE("'.$find_and_delete.'",id_vk)', array());
//        //$res = $db->select('select * from new_year_clients where LOCATE("'.$find_and_delete.'",id_vk)', array());
//        //var_dump($res);
//        echo '<br>';
//    }
//}
//
echo 'ok';