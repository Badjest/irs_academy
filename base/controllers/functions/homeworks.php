<?

function homeworks() {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }


    $homeworks = $db->select(
        'select 
 homeworks.id_user_lesson,
 user_lessons.last_hw,
 user_lessons.teacher_look,
 lessons.name,
 cources.name as courcename 
 from homeworks
join user_lessons on homeworks.id_user_lesson = user_lessons.id
join lessons on user_lessons.id_lesson = lessons.id
join cources on lessons.cource_id = cources.id
group by homeworks.id_user_lesson
ORDER BY user_lessons.teacher_look ASC, 
user_lessons.last_hw DESC'
    );

    for ($i = 0; $i < count($homeworks); $i++) {
        $id_us_l = $homeworks[$i]['id_user_lesson'];
        $user = $db->select('SELECT users.username, users.userlastname 
FROM users join user_lessons on user_lessons.id_user = users.id WHERE user_lessons.id = {?}', array($id_us_l));
        $homeworks[$i]['username'] = $user[0]['username'];
        $homeworks[$i]['userlastname'] = $user[0]['userlastname'];

    }



    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Домашние работы',
            'username' => $username
        )
    );
    echo loadView('templates/homeworks.php',array(
        'homeworks'=> $homeworks
    ));
    echo loadView('templates/footer.php',array());
}