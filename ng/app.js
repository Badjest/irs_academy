var app = angular.module("app", ["ngCookies"]);

app.controller("PublicController", function($scope, $rootScope) {


    $scope.notif = 'MAIL';

    $scope.init = function (ROOT, BASE_LINK) {
        $rootScope.ROOT = ROOT;
        $rootScope.BASE_LINK = BASE_LINK;

        $.get('/checkNotif', function (data) {
            var res = JSON.parse(data);
            if (res.error) {
                $('.bg-modal').show();
                $('.modalBox').show(300);
            }
        });
    };

    $scope.saveCheckNotif = function () {

        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + "/saveCheckForm",
            data    : { select: $scope.notif }
        }).success(function(data) {
            data = JSON.parse(data);
            if (data.success) {
                $('.bg-modal').hide();
                $('.modalBox').hide(300);
            } else {
                $('.bg-modal').hide();
                $('.modalBox').hide(300);
                error($scope.notif);
            }
        });


    };


});


// LoginController
app.controller("LoginController", function($scope,$rootScope) {
    $scope.loginFormData = {};
    $scope.loginError = "";
    $scope.loader = $('.loader .line');



    $scope.loginSubmit = function(e) {
        e.preventDefault();

        if (!$scope.loginFormData.email || !$scope.loginFormData.password) {
            error('Логин или пароль не может быть пустым!');
            return;
        }

        if ($scope.loginFormData.email === '' || $scope.loginFormData.password === '') {
            error('Логин или пароль не может быть пустым!');
            return;
        }


        $.ajax({
            method  : "POST",
            url     : "login",
            data    : $.param($scope.loginFormData),
            beforeSend : function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '50%');
            }
        }).success(function(data) {
            data = JSON.parse(data);
            if (data.success) {
                $scope.loader.css('width', '100%');
                window.location.href = $rootScope.ROOT + "/my";
            } else {
                $scope.loader.css('opacity', '0');
                $scope.loader.css('width', '0%');
                error(data.message);
            }
        });

    }


});

app.controller("myLessons", function($scope) {
    $scope.loader = $('.loader .line');
    $scope.cources = {};

    $scope.init = function(source)
    {
        $scope.cources = source;

        for (var i = 0; i < $scope.cources.length; i++) {
            var proc =  ($scope.cources[i].complete_lesson/$scope.cources[i].all_lesson) * 100;
            $scope.cources[i].proc = proc;
        }

    };

});


app.controller("forgetPass", function($scope) {
    $scope.loader = $('.loader .line');
    $scope.mail = '';

    $scope.passRecover = function(e) {
        e.preventDefault();

        $.ajax({
            method  : "POST",
            url     : "recoverPass",
            data    :  {mail: $scope.mail},
            beforeSend : function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '50%');
            }
        }).success(function(data) {
            data = JSON.parse(data);
            if (data.success) {
                $scope.loader.css('width', '100%');
                $scope.loader.css('opacity', '0');
                success('На указанный почтовый ящик отправлен новый пароль для входа');
            } else {
                $scope.loader.css('opacity', '0');
                $scope.loader.css('width', '0%');
                error(data.message);
            }
        });

    }
});


app.controller("HeaderController", function($scope, $cookies, $rootScope) {
    $scope.loader = $('.loader .line');

    $scope.exitUser = function(e) {
        e.preventDefault();
        $cookies.remove("hash", { path: $rootScope.ROOT });
        $scope.loader.css('width', '100%');
        window.location.href = $rootScope.ROOT +'/auth';
    }
});


app.controller("SettingController", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.userData = {};
    $scope.passwords = {
        new_confirm:'',
        new:''
    };
    $scope.secondPassField = $('#secondPassField');
    $scope.confirm = true;


    $scope.init = function(source)
    {
        $scope.userData = source;
    };

    $scope.confirmPass = function()
    {
        if ($scope.passwords.new === '') {
            $scope.secondPassField.css('border', '1px solid #d9d9d9');
            $scope.confirm = true;
        }
        if ($scope.passwords.new_confirm === '') {
            $scope.secondPassField.css('border', '1px solid #d9d9d9');
            return;
        }

        if ($scope.passwords.new === $scope.passwords.new_confirm) {
            $scope.secondPassField.css('border', '1px solid green');
            $scope.confirm = true;
        } else {
            $scope.secondPassField.css('border', '1px solid red');
            $scope.confirm = false;
        }
    };


    $scope.saveSetting = function(e) {

        if (typeof e !== 'undefined') {
            e.preventDefault();
        }


        if (!$scope.confirm) {
            error('Пароли не совпадают!');
            return;
        }

        if ($scope.passwords.new != '') {
            $scope.userData.pass = $scope.passwords.new;
        }


        $.ajax({
            method  : "POST",
            url     :  $rootScope.ROOT + "/saveUser",
            data    : $.param($scope.userData),
            beforeSend : function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '50%');
            }
        }).success(function(data) {
            data = JSON.parse(data);
            if (data.success) {
                $scope.loader.css('width', '100%');
                $scope.loader.css('opacity', '0');
                success('Данные сохранены');
                $scope.passwords.new = '';
                $scope.passwords.new_confirm = '';
            } else {
                $scope.loader.css('opacity', '0');
                $scope.loader.css('width', '0%');
                error(data.message);
            }

        });
    };


    VK.Widgets.AllowMessagesFromCommunity("vk_send_message", {height: 30}, 151506931);

    VK.Observer.subscribe("widgets.allowMessagesFromCommunity.allowed", function f(userId) {
        $scope.userData.vk_notif = '1';
        $scope.userData.vk = userId;
        $scope.saveSetting();
        $scope.$apply();
    });

    VK.Observer.subscribe("widgets.allowMessagesFromCommunity.denied", function f(userId) {
        $scope.userData.vk_notif = '0';
        $scope.userData.vk = userId;
        $scope.saveSetting();
        $scope.$apply();
    });


});


app.controller("LessonsController", function($scope, $sce) {
    $scope.loader = $('.loader .line');
    $scope.complete_line = $('.project-process .line');
    $scope.lessons = {};
    $scope.cource ={};


    $scope.data = {
        complete:0,
        all: 0
    };

    $scope.init = function(lessons,cource)
    {
        $scope.lessons = lessons;
        $scope.cource = cource;
        $scope.data.all = lessons.length;
        for (var i = 0; i < lessons.length; i++) {
            if ($scope.lessons[i].stage === "ACCESS") {
                $scope.data.complete++;
            }
            $scope.lessons[i].name = $sce.trustAsHtml($scope.lessons[i].name);
        }
        var proc =  ($scope.data.complete/$scope.data.all) * 100;
        $scope.complete_line.css('width',proc+'%');
    };

});

app.controller("LessonController", function($scope, $sce, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.lesson = {};
    $scope.cource ={};
    $scope.files = [];
    $scope.user_lesson_id = '';
    $scope.id_user = '';
    $scope.process = false;
    $scope.try = [];

    $scope.websocket = null;

    if (typeof io !== 'undefined') {
        $scope.socket = io.connect('https://irs.academy:8091');

        $scope.socket.on('updatechat', function (username, message, date, id) {
            var messageField = $('.message-wrapp');

            if (id === $scope.id_user) {
                var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+username+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+date+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+message+'\
                        </div>\
                        </div>\
                        </div>';
                messageField.append(messageTo);
            } else {
                var messageFrom = '<div class="message-from clearfix">\
                    <div class="col-xs-5 "><div class="date">'+date+'</div></div>\
                    <div class="col-xs-7 "><span class="name_from">'+username+'</span></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-from">'+message+'\
                        </div>\
                        </div>\
                        </div>';
                messageField.append(messageFrom);

            }
            messageField.scrollTop(messageField.prop("scrollHeight"));
        });

    }


    $scope.init = function(lessons,cource, user_lesson_id, ar_try, username)
    {

        $scope.lesson = lessons;
        $scope.cource = cource;

        var proc =  ($scope.cource.complete_lesson/$scope.cource.all_lesson) * 100;
        $scope.cource.proc = proc;

        if ($scope.lesson.video instanceof Array) {
            for (var i = 0; i < $scope.lesson.video.length; i++) {
                $scope.lesson.video[i].path = $sce.trustAsResourceUrl($scope.lesson.video[i].path);
            }
        } else {
            $scope.lesson.video = [];
        }
        $scope.user_lesson_id = user_lesson_id;
        $scope.lesson.content = $sce.trustAsHtml($scope.lesson.content);
        $scope.id_user = $scope.cource.id_user;
        $scope.username = username;
        $scope.try = ar_try;
        var messageField = $('.message-wrapp');
        messageField.scrollTop(messageField.prop("scrollHeight"));



        if (typeof io !== 'undefined') {
            $scope.socket.on('connect', function () {
                $scope.socket.emit('adduser', $scope.username, $scope.user_lesson_id, $scope.id_user);
            });
        }



    };



    $('.message-area .textTo').keydown(function (e) {
        if ((e.ctrlKey || e.metaKey) && (e.keyCode == 13 || e.keyCode == 10)) {
            $scope.sendMessage(e);
        }
    });





    $scope.sendMessage = function (e) {
        e.preventDefault();
        var fieldSend = $('.textTo');
        var message = fieldSend.val();
        if (message === '') {
            error('Нельзя отправить пустое сообщение!');
            return false;
        }
        message = message.replace(/\n/g, '<br>');
        fieldSend.val('');
        if (typeof io !== 'undefined') {
            $scope.socket.emit('sendchat', message);
        } else {
            error('Ошибка чата, извините.');
        }
    };

    $scope.sendCheck = function (e) {
        e.preventDefault();
        var report = $('#editor').val();
        var fd = new FormData();
        for (var i =0; i < $scope.files.length; i++) {
            fd.append('file-'+i,$scope.files[i]);
        }
        fd.append('content',report);
        fd.append('id_user_lesson',$scope.user_lesson_id);

        if (!$scope.process) {
            $.ajax({
                method: "POST",
                url: $rootScope.ROOT + '/sendData',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $scope.loader.css('opacity', '1');
                    $scope.loader.css('width', '100%');
                    $scope.process = true;
                }
            }).success(function (data) {
                var result = JSON.parse(data);

                if (result.success) {
                    successCenter('Дорогой друг, проверка домашних заданий проходит каждое четное число месяца, исключая воскресенье.' +
                        'При этом, допуск к следующему уроку можно попросить открыть сразу после отправки дз.');
                    $scope.files = [];
                    $('#editor').val('');
                    $('.report-area').remove();
                    $scope.loader.css('opacity', '0');
                    $scope.loader.css('width', '0');
                    $scope.$apply();
                    $scope.process = false;
                }
            });
        }
    };

    $scope.uploadImage = function(images) {
        for (var i =0; i< images.length; i++) {
            $scope.files.push(images[i]);
        }
        $scope.$apply();
    };

    $scope.delFile = function(index) {
        $scope.files.splice(index, 1);
        $scope.$apply();
    };
});


app.controller("adminUser", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.ROOT = '';
    $scope.users = [];

    $scope.init = function(users, root)
    {
        $scope.users = users;
        $scope.ROOT = root;
    };

    $scope.addUser = function(e)
    {
        e.preventDefault();
        $scope.users.unshift({
            username: '',
            mail: '',
            pass:'',
            phone:'',
            userlastname:'',
            admin: '0'
        });
    };

    $scope.saveSet = function(e)
    {
        e.preventDefault();
        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + "/saveUsers",
            data    : {'users' :  $scope.users},
            beforeSend : function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
            }
        }).success(function(data) {
            console.log(data);
            data = JSON.parse(data);
            if (data.success) {
                window.location.reload();
            } else {
                $scope.loader.css('opacity', '0');
                $scope.loader.css('width', '0%');
                error(data.message);
            }
        });
    };

});


app.controller("editCourceController", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.cource = {};
    $scope.lessons = [];
    $scope.users = [];
    $scope.files = '';

    $scope.init = function(cource,lessons, users, root)
    {
        $scope.cource = cource;
        $scope.lessons = lessons;
        $scope.users = users;
        $rootScope.ROOT = root;
    };

    $scope.addLesson = function(e)
    {
        e.preventDefault();
        $scope.lessons.push({
            name:'',
            del: true
        });
    };

    $scope.delLes = function(index,e)
    {
        e.preventDefault();

        if ($scope.lessons[index].del) {
            $scope.lessons.splice(index, 1);
            $scope.$apply();
            return false;
        }

        noty({
            text: 'Вы уверены, что хотите удалить уроки? Вместе с ним удаляются все сообщения пользователй по данному уроку, домашение задания, файлы!',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Да, удалить', onClick: function($noty) {

                    $.ajax({
                        method  : "POST",
                        url     : $rootScope.ROOT +'/delLesson/'+ $scope.lessons[index].id
                    }).success(function(data) {
                        var result = JSON.parse(data);
                        if (result.success) {
                            success(result.message);
                            $scope.lessons.splice(index, 1);
                            $scope.$apply();
                        } else {
                            error(result.message);
                        }
                    });

                }
                },
                {addClass: 'btn btn-danger', text: 'Отмена', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });




    };

    $scope.uploadImage = function(image) {
        $scope.files = image;
    };

    $scope.saveSet = function(e)
    {
        e.preventDefault();
        var fd = new FormData();
        if ($scope.files != '') {
            fd.append('pic',$scope.files[0]);
        }
        fd.append('id_cource', $scope.cource.id);
        fd.append('cource_name', $scope.cource.name );
        fd.append('lessons',JSON.stringify($scope.lessons));
        //fd.append('users',JSON.stringify($scope.users));

        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT +'/saveCource',
            data    : fd,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $scope.loader.css('width', '100%');
            }
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                success('Сохранено!');
                //window.location.href = $rootScope.Root + '/admin_cources/edit/'+ result.message;
                window.location.reload();
            }
        });

    };

});


app.controller("LessonControllerAdmin", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.files = '';
    $scope.lesson = {};


    $scope.uploadFile = function(image) {

        var fd = new FormData();

        fd.append('file',image[0]);

        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT +'/uploadFile',
            data    : fd,
            cache: false,
            contentType: false,
            processData: false
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                $('.redactor_box div').append('<p><a href="'+result.message+'">Ссылка на файл</a></p>');
                success('Ссылка добавлена в редактор!');
            } else {
                error(result.message);
            }
        });
    };

    $scope.init = function(lesson)
    {
        $scope.lesson = lesson;
        if ($scope.lesson.video === '') {
            $scope.lesson.video = [];
        }

    };



    $scope.addVideo = function () {
        $scope.lesson.video.push(
            {path : 'https://www.youtube.com/embed/',
                name:'',
                sort:0}
        );
    };

    $scope.delVideo = function (i) {
        $scope.lesson.video[i].del = 1;
        $scope.apply();
    };

    $scope.saveSet = function(e)
    {
        e.preventDefault();
        var fd = new FormData();
        if ($scope.files != '') {
            fd.append('pic',$scope.files[0]);
        }
        var content = $('#editor').val();
        $scope.lesson.content = content;

        fd.append('lesson',JSON.stringify($scope.lesson));

        $.ajax({
            method  : "POST",
            url     :  $rootScope.ROOT + '/saveLesson',
            data    : fd,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $scope.loader.css('width', '100%');
            }
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                success('Сохранено!');
                window.location.reload();
            } else {
                error(result.message);
            }
        });

    };


});

app.controller("AdminCources", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.cources = [];

    $scope.init = function(cources, root)
    {
        $scope.cources = cources;
        $rootScope.ROOT = root;
    };

    $scope.deleteCource = function (e, id) {
        e.preventDefault();
        noty({
            text: 'Вы уверены, что хотите удалить курс? Вместе с ним удаляются все уроки!',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Да, удалить', onClick: function($noty) {

                    $noty.close();
                    $.ajax({
                        method  : "POST",
                        url     : $rootScope.ROOT + '/delCource',
                        data    : {id_cource:id},
                        beforeSend: function () {
                            $scope.loader.css('width', '100%');
                        }
                    }).success(function(data) {
                        var result = JSON.parse(data);
                        if (result.success) {
                            success('Удалено!');
                            window.location.reload();
                        } else {
                            error(result.message);
                        }
                    });

                }
                },
                {addClass: 'btn btn-danger', text: 'Отмена', onClick: function($noty) {
                    $noty.close();
                }
                }
            ]
        });
    };


});


app.controller("AdminHomeworks", function($scope, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.homeworks = [];


    $scope.init = function(homeworks)
    {
        $scope.homeworks = homeworks;
    };
});

app.controller("HomeController", function($scope, $sce, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.homeworks = [];
    $scope.homework_status = {};
    $scope.other_lessons = [];
    $scope.info = {};
    $scope.user_lesson_id = 0;
    $scope.username = "";

    $scope.socket = io.connect('https://irs.academy:8091');

    $scope.init = function(homeworks,homework_status,other_lessons, chatroom, id_user, info)
    {
        $scope.homeworks = homeworks;

        for (var i = 0; i < $scope.homeworks.length; i++) {
            $scope.homeworks[i].content = $sce.trustAsHtml($scope.homeworks[i].content);
        }
        $scope.homework_status= homework_status;
        $scope.other_lessons = other_lessons;


            for (var j = 0; j < $scope.other_lessons.length; j++) {
                $scope.other_lessons[j].sort = parseInt($scope.other_lessons[j].sort);
            }


        $scope.user_lesson_id = chatroom;
        $scope.id_user = id_user;
        $scope.info = info;
    };


    $scope.noRead = function (id_user_lesson) {
        console.log(id_user_lesson);
        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + '/changeStatusHomeworkRead/' + id_user_lesson
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                $scope.info.teacher_look = '0';
                $scope.$apply();
            } else {
                error(result.message);
            }
        });
    };


    $scope.socket.on('connect', function(){
        $scope.socket.emit('adduser', $scope.info.name_admin , $scope.user_lesson_id , $scope.id_user);
    });




    $scope.socket.on('updatechat', function (username, message, date, id) {
        var messageField = $('.message-wrapp');

        if (id === $scope.id_user) {
            var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+username+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+date+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+message+'\
                        </div>\
                        </div>\
                        </div>';
            messageField.append(messageTo);
        } else {
            var messageFrom = '<div class="message-from clearfix">\
                    <div class="col-xs-5 "><div class="date">'+date+'</div></div>\
                    <div class="col-xs-7 "><span class="name_from">'+username+'</span></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-from">'+message+'\
                        </div>\
                        </div>\
                        </div>';
            messageField.append(messageFrom);

        }
        messageField.scrollTop(messageField.prop("scrollHeight"));
    });



    $('.message-area .textTo').keydown(function (e) {
        if ((e.ctrlKey || e.metaKey) && (e.keyCode == 13 || e.keyCode == 10)) {
            $scope.sendMessage(e);
        }
    });



    $scope.sendMessage = function (e) {
        e.preventDefault();
        var fieldSend = $('.textTo');
        var message = fieldSend.val();
        message = message.replace(/\n/g, '<br>');
        $scope.socket.emit('sendchat', message);
        fieldSend.val('');
    };

    $scope.saveMark = function(item_homework) {
        console.log(item_homework);
        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + '/setMarkToTry',
            data    : {
                mark : item_homework.mark,
                id_homework : item_homework.id,
                courcename : $scope.info.courcename,
                id_user : $scope.id_user
            },
            beforeSend: function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
            }
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                success('Сохранено!');
            } else {
                error(result.message);
            }
            $scope.loader.css('opacity', '0');
            $scope.loader.css('width', '0%');
        });
    };



    $scope.saveSet = function () {
        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + '/saveStatusLesson',
            data    : {homework:$scope.homework_status, other_lessons:$scope.other_lessons},
            beforeSend: function () {
                $scope.loader.css('width', '100%');
            }
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                success('Сохранено!');
            } else {
                error(result.message);
            }
            $scope.loader.css('width', '0%');
            $scope.loader.css('opacity', '0');
        });
    };

});


app.controller("WebinarController", function($scope, $cookies, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.user = '';
    $scope.webinar = {};
    $scope.hash = undefined;
    $scope.modal = {
        name: '',
        last_name: '',
        number:'',
        email: '',
        part : 'Оплатите полный курс',
        option : 'full'
    };
    $scope.transNow = false;
    $scope.player = null;



    $scope.init = function(webinar)
    {
        $scope.webinar = webinar;
        $scope.user = $cookies.get('user');
        $scope.hash = $cookies.get('hash');


        if ($scope.user === undefined) {
            $('.bg-modal').show(300);
            $('.modalBox2').show(300);
        } else {
            $scope.initSocket();
        }

        if ($scope.hash === undefined) {
            $scope.hash  = Math.random().toString(36).substring(9);
            var expireDate = new Date();
            expireDate.setDate(expireDate.getDate() + 900);
            $cookies.put("hash", $scope.hash ,{ path: $rootScope.ROOT , 'expires': expireDate});
        }

        var height = $(window).height() - 320 - $('.chat-title .names').height();
        if ($(window).height() < 600) {
            $('.rez').css('height', 600);
        } else {
            $('.rez').css('height', height);
        }

        $scope.loadPlayer();

    };


    $(window).resize(function () {
        var height = $(window).height() - 320 - $('.chat-title .names').height();
        if ($(window).height() < 600) {
            $('.rez').css('height', 600);
        } else {
            $('.rez').css('height', height);
        }
    });


    $scope.loadPlayer = function () {
        if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            window.onYouTubePlayerAPIReady = function () {
                $scope.onYouTubePlayer();
            };

        } else {
            $scope.onYouTubePlayer();
        }
    };


    $scope.openVideoAndPlay = function () {
        $('.webinar-wrapp').show(400);
        $('#imgZagl').hide(400);
        $('.video-less').click();
        $('.mobileNW').show();
        $scope.player.seekTo(1);
        //$scope.player.playVideo();
        $scope.transNow = true;
    };

    $scope.hideVideoAndStop = function () {
        $('.webinar-wrapp').hide(400);
        $('#imgZagl').show(400);
        $('.mobileNW').hide();
        $scope.player.stopVideo();
        $scope.transNow = false;
    };

    $scope.onYouTubePlayer = function () {
        $scope.player = new YT.Player('video-webinar', {
            events: {
                'onReady': onPlayerReady
            }
        });
    };


    function onPlayerReady(event) {

        var nowTime = moment( $scope.webinar.date_now , "HH:mm:ss");
        var startTime = moment($scope.webinar.start_video, "HH:mm:ss");
        var endTime = moment($scope.webinar.end_video, "HH:mm:ss");
        var duration = moment.duration(nowTime.diff(startTime));
        var durationEnd = moment.duration(nowTime.diff(endTime));

        var sec = duration.asSeconds();
        var secEnd = durationEnd.asSeconds();

        if (sec > 0 && secEnd < 0) {
            $scope.player.seekTo(sec);
            $scope.transNow = true;
        }
    }


    $scope.mobileNW = function () {
        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + '/getDateNow'
        }).success(function(data) {
            $scope.webinar.date_now = data;
            var nowTime = moment( $scope.webinar.date_now , "HH:mm:ss");
            var startTime = moment($scope.webinar.start_video, "HH:mm:ss");
            var endTime = moment($scope.webinar.end_video, "HH:mm:ss");
            var duration = moment.duration(nowTime.diff(startTime));
            var durationEnd = moment.duration(nowTime.diff(endTime));

            var sec = duration.asSeconds();
            var secEnd = durationEnd.asSeconds();

            if (sec > 0 && secEnd < 0) {
                event.target.seekTo(sec);
                $scope.transNow = true;
                //event.target.playVideo();
            }
        });
    };




    $scope.saveUser = function()
    {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 900);
        $cookies.put("user", $scope.user ,{ path: $rootScope.ROOT , 'expires': expireDate});
        $('.bg-modal').hide(300);
        $('.modalBox2').hide(300);
        $scope.initSocket();
        //$scope.$apply();
    };

    $scope.openModal = function(state)
    {
        if (state === 'full') {
            $scope.modal.part = 'Оплатите полный курс';
            $scope.modal.option = 'full';
        }
        if (state === 'rassrochka') {
            $scope.modal.part = 'Внесите предоплату за курс';
            $scope.modal.option = 'rassrochka';
        }
        $('.bg-modal').show(300);
        $('.modalBox3').show(300);
    };

    $scope.saveModal = function () {
        $('input[ng-model="modal.name"]').css('border', '1px solid #d9d9d9');
        $('input[ng-model="modal.number"]').css('border', '1px solid #d9d9d9');
        $('input[ng-model="modal.email"]').css('border', '1px solid #d9d9d9');
        if ($scope.modal.name.length < 2) {
            $('input[ng-model="modal.name"]').css('border', '1px solid red');
            return false;
        }
        if ($scope.modal.number.length < 6) {
            $('input[ng-model="modal.number"]').css('border', '1px solid red');
            return false;
        }
        if ($scope.modal.email.length < 3) {
            $('input[ng-model="modal.email"]').css('border', '1px solid red');
            return false;
        }


        var utm_content = $cookies.get('utm_content');
        var utm_medium = $cookies.get('utm_medium');


        if (utm_content) {
            $scope.modal.utm_content = utm_content;
        }

        var utm = $('#utmid').val();
        if (utm === '') {
            $scope.modal.utmid = 0;
        } else {
            $scope.modal.utmid = utm;
        }

        if (utm_medium) {
            $scope.modal.utmid = utm_medium;
        }

        $scope.modal.webname = $scope.webinar.name;

        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT + '/sendWebinarModal',
            data    : {modal: $scope.modal},
            beforeSend: function () {
                $scope.loader.css('width', '100%');
            }
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                //location.href = result.message;
                for (let prop in result.message) {
                    $('input[name="'+prop+'"]').val(result.message[prop]);
                }

                $('#payForm').trigger('submit');


            } else {
                error(result.message);
            }
            $scope.loader.css('width', '0%');
            $scope.loader.css('opacity', '0');
        });

    };

    $scope.openButtons = function () {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 900);
        $cookies.put("openButton_" + $scope.webinar.url, true ,{ path: $rootScope.ROOT , 'expires': expireDate});
        $('.buttons-block').show(500);
    } ;

    $scope.closeButtons = function () {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 900);
        $cookies.put("openButton_" + $scope.webinar.url, false ,{ path: $rootScope.ROOT , 'expires': expireDate});
        $('.buttons-block').hide(500);
    } ;

    $scope.openButtonsAdmin = function () {
        $scope.socket.emit('openAllButtons');
    };

    $scope.closeButtonsAdmin = function () {
        $scope.socket.emit('closeAllButtons');
    };

    $('.bg-modal').on('click', function () {
        $(this).hide(300);
        $('.modalBox3').hide(300);
    });

    $scope.initSocket = function () {
        $scope.socket = io.connect('https://irs.academy:8098');


        $scope.socket.on('connect', function(){
            $scope.socket.emit('adduser', $scope.user , $scope.webinar.url);
        });


        $scope.socket.on('updatechat', function (username, message, date , hash) {

            if (hash === $scope.hash) return false;

            var messageField = $('.message-wrapp');

            var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+username+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+date+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+message+'\
                        </div>\
                        </div>\
                        </div>';
            messageField.append(messageTo);

            messageField.scrollTop(messageField.prop("scrollHeight"));
        });

        $scope.socket.on('openButtons', function () {
            $scope.openButtons();
        });

        $scope.socket.on('closeButtons', function () {
            $scope.closeButtons();
        });


        $scope.socket.on('closeVideo', function () {
            $scope.hideVideoAndStop();
        });

        $scope.socket.on('openVideo', function () {
            $scope.openVideoAndPlay();
        });

        $scope.sendMessage = function (e) {
            e.preventDefault();

            if (!$scope.transNow) {
                error('Чат временно недоступен! Дождитесь начала трансляции.');
                //return false;
            }
            var fieldSend = $('.textTo');
            var message = fieldSend.val();
            //$scope.socket.emit('sendchat', message);

            var messageField = $('.message-wrapp');


            var options = {
                    timeZone: 'Asia/Novosibirsk',
                    hour: 'numeric', minute: 'numeric', second: 'numeric'
                },
                formatter = new Intl.DateTimeFormat([], options);
            var time_now = formatter.format(new Date());


            var messageTo = '<div class="message-to clearfix">\
                        <div class="col-xs-7 npr">'+$scope.user+'</div>\
                    <div class="col-xs-5 npl"><div class="date">'+time_now+'</div></div>\
                    <div class="col-xs-12">\
                        <div class="message-text message-text-to">'+message+'\
                        </div>\
                        </div>\
                        </div>';

            messageField.append(messageTo);


            messageField.scrollTop(messageField.prop("scrollHeight"));
            fieldSend.val('');

            $.ajax({
                method  : "POST",
                url     : '/saveWebinarMessage',
                data    : {name: $scope.user, message: message, url : $scope.webinar.url, hash: $scope.hash }
            }).success(function(data){

                $scope.socket.emit('preModer', $scope.user, message, $scope.hash);

            });
        };
    };



});


app.controller("WebinarAdminController", function($scope, $cookies, $rootScope) {
    $scope.loader = $('.loader .line');
    $scope.webinar = {};
    $scope.messages = {};

    $scope.init = function(webinar, messages)
    {
        $scope.webinar = webinar;
        $scope.messages = messages;
    };

    $scope.addMessage = function()
    {
        $scope.messages.push({
            name:'',
            message: '',
            time: '12:00:00',
            del: true
        });
    };


    $scope.delMessage = function (index) {
        if ($scope.messages[index].del) {
            $scope.messages.splice(index, 1);
            $scope.$apply();
            return false;
        }

        $.ajax({
            method  : "POST",
            url     : $rootScope.ROOT +'/delWebinarMessage/'+ $scope.messages[index].id
        }).success(function(data) {
            var result = JSON.parse(data);
            if (result.success) {
                success(result.message);
                $scope.messages.splice(index, 1);
                $scope.$apply();
            } else {
                error(result.message);
            }
        });
    };

    $scope.saveSet = function (e) {
        e.preventDefault();

        $.ajax({
            method: "POST",
            url: $rootScope.ROOT + "/saveWebinar",
            data: {
                'webinar': $scope.webinar ,
                'messages': $scope.messages
            },
            beforeSend: function () {
                $scope.loader.css('opacity', '1');
                $scope.loader.css('width', '100%');
                $scope.loading = true;
            }
        }).success(function (data) {
            data = JSON.parse(data);
            if (data.success) {
                success('Сохранено');
                window.location.reload();
            } else {
                error(data.message);
            }
            $scope.loader.css('opacity', '0');
            $scope.loader.css('width', '0%');
        });
    };



});

function error(message)
{
    noty({
        text: message ,
        timeout: 5000,
        type:'error',
        layout: 'topRight'
    });
}

function success(message)
{
    noty({
        text: message,
        timeout: 5000,
        type:'success',
        layout: 'topRight'
    });
}

function successCenter(message)
{
    noty({
        text: message,
        timeout: 10000,
        type:'success',
        layout: 'center'
    });
}
