<?

function admin_cource() {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $cources = $db->select('Select * from cources');

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Управление курсами',
            'username' => $username
        )
    );
    echo loadView('templates/admin_course.php',array(
        'cources'=> $cources
    ));
    echo loadView('templates/footer.php',array());
}