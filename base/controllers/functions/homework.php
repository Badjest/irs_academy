<?

function homework($id_users_lesson) {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT id, admin FROM users where hash= {?}', array($hash));
        $id_user = $result[0]['id'];
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $homeworks = $db->select('select * from homeworks WHERE homeworks.id_user_lesson = {?}',
    array($id_users_lesson));

    for ($i = 0; $i < count($homeworks); $i++ ) {
        $content =  $db->select('select * from users_content where id_homework = {?}',
            array($homeworks[$i]['id']));
        $homeworks[$i]['files'] = $content;
    }

    $info_about_cource = $db->select('SELECT 
cources.id as cource_id, 
users.id as user_id,
lessons.name,
cources.name as courcename,
users.username,
users.userlastname,
user_lessons.pay
FROM user_lessons
JOIN lessons on user_lessons.id_lesson = lessons.id
JOIN users on user_lessons.id_user = users.id
JOIN cources on lessons.cource_id = cources.id
WHERE user_lessons.id = {?}',array($id_users_lesson));

    $info_about_cource = $info_about_cource[0];

    $homework_status = $db->select('select id, stage, teacher_look from user_lessons where id={?}',
        array($id_users_lesson));

    $info_about_cource['teacher_look'] = $homework_status[0]['teacher_look'];

    $other_lessons = $db->select('
SELECT lessons.name, 
lessons.sort,
user_lessons.stage, 
user_lessons.id,
user_lessons.pay
FROM user_lessons 
join lessons on user_lessons.id_lesson = lessons.id 
JOIN cources on lessons.cource_id = cources.id
where 
cources.id = {?} AND user_lessons.id_user = {?} AND user_lessons.id <> {?}',
        array($info_about_cource['cource_id'],$info_about_cource['user_id'], $homework_status[0]['id']));

    $messages = $db->select('select date_send, message, id_user_lesson, user_id, users.username 
from messages join users on messages.user_id = users.id WHERE id_user_lesson = {?} 
order by date_send ASC', array($id_users_lesson));

    for($i=0;$i<count($messages);$i++) {
        $messages[$i]['message'] = base64_decode($messages[$i]['message']);
    }

    $db->query('update user_lessons set teacher_look = 1 where id = {?}',
        array($id_users_lesson));

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Домашняя работа',
            'username' => $username,
            'chat_on' => true
        )
    );
    $info_about_cource['name_admin'] = $username;
    $info_about_cource['id_user_lesson'] = $id_users_lesson;
    echo loadView('templates/homework.php',array(
        'homeworks'=> $homeworks,
        'homework_status' => $homework_status[0],
        'other_lessons' => $other_lessons,
        'messages' => $messages,
        'id_room' => $id_users_lesson,
        'id_user' => $id_user,
        'info' => $info_about_cource
    ));
    echo loadView('templates/footer.php',array());
}