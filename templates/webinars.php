<? $data = $arrayOfData;?>
<div class="container" ng-controller="AdminWebinars" ng-init="init(<?php echo htmlspecialchars(json_encode($data['webinars'])); ?>)">

	<a class="btn fly" ng-click="newWebinar()">Новый<br>вебинар</a>
    <a class="btn fly-save" ng-click="saveWebinars()">Сохранить</a>

	<div class="col-md-12 col-sm-12 col-xs-12">
		

		<h3 class="def-title">Все вебинары</h3>
			

		<div class="my-courses">
			
			<div ng-repeat="item in webinars track by item.id"  class="course-item clearfix">
				
				<div class="col-md-10 col-xs-12">
                    <div class="col-md-9">
                        <input type="text" ng-model="item.name" placeholder="Название вебинара">
                        <input type="text" ng-model="item.url" placeholder="ЧПУ">
                        <span>
						<div style="width: 240px; color:red;" ng-click="deleteWebinar($index)" href="">Удалить вебинар</div>
					</span>
                        <span>
						<a ng-if="item.id" href="<?=ROOT?>/webinar/{{item.url}}" href="">Перейти</a>
					</span>
                    </div>


				</div>

			</div>

		</div>

	</div>

</div>