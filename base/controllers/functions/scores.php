<?

function scores() {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }




    $users = $db->select('SELECT username, SUM(best) as top 
FROM (SELECT CONCAT(users.username, " ", users.userlastname ) as username, 
user_lessons.id_user as user, MAX(homeworks.mark) as best , homeworks.id_user_lesson
 FROM homeworks JOIN user_lessons on homeworks.id_user_lesson = user_lessons.id 
 JOIN users on user_lessons.id_user = users.id GROUP BY user_lessons.id) as bests 
 GROUP BY user ORDER BY top DESC');



    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Рейтинги',
            'username' => $username
        )
    );
    echo loadView('templates/scores.php',array(
        'users'=> $users
    ));
    echo loadView('templates/footer.php',array());
}