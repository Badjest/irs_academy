<? $data = $arrayOfData;?>
<div class="container" ng-controller="myLessons" ng-init="init(<?php echo htmlspecialchars(json_encode($data['cources'])); ?>)">

    <div class="warning-about">
        Внимание! Если вы хотите получать уведомления через VK.com, пожалуйста, подтвердите свой аккаунт в <a
                href="<?=ROOT?>/settings">настройках</a>
    </div>


    <div class="col-md-12 col-sm-12 col-xs-12">
		

		
		<h3 class="def-title">Мои курсы</h3>
			

		<div class="my-courses">
			
			<a ng-repeat="item in cources track by item.id" href="my/{{item.id}}" class="course-item clearfix">
				
				<div class="col-md-10 col-xs-12">
					<img src="{{item.photo}}" alt="">
					<span>{{item.name}}</span>
				</div>

				<div class="col-md-2 col-xs-12">
					<div class="right-side-item">
						Кол-во: <br>
						{{item.count_lessons}} уроков
					</div>
				</div>

			</a>

		</div>

	</div>

</div>