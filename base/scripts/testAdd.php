<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);


$price = '5200';
$phone = '34234234';
$name = 'Тестирую скрипт';
$mail = 'impavelsukhinin@yandex.ru';

$config_db = include "/academy/base/config/config_db.php";

$db = Db::getDB($config_db);

$pass = generateRandomString(8);

$insert_id = $db->query('insert ignore into users set username = {?} ,
                        mail = {?}, pass = {?}, phone = {?}, userlastname= {?},
                        admin = {?}',
    array($name,$mail, $pass, $phone, '', 0));


if ($insert_id) {


    if ($price === '5200') {

        $cources = $db->select('select id from cources where id = 1 or id = 5');

        foreach ($cources as $cource) {
            $db->query('insert into user_cources set id_user = {?}, id_cource = {?}',
                array($insert_id, $cource['id']));
        }


        $lessons = $db->select('select * from lessons where cource_id = 1 ORDER BY sort ASC');

        $i = 0;
        foreach ($lessons as $lesson) {

            if ($i === 0 || $i === 1) {
                echo 'Полное открытие <br>';
                var_dump($db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="NEW" , pay = 1',
                    array($lesson['id'], $insert_id)));

            } else {
                echo 'Частичное открытие <br>';
                var_dump($db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="CLOSE" , pay = 1',
                    array($lesson['id'], $insert_id)));
            }
            $i++;
        }


        $lessons = $db->select('select * from lessons where cource_id = 5 ORDER BY sort ASC');
        $is_first = true;

        echo  '<br>5 lesson';
        foreach ($lessons as $lesson) {

            if ($is_first) {
                $db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="NEW" , pay = 1 ',
                    array($lesson['id'], $insert_id));
            } else {
                $db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="CLOSE" , pay = 1 ',
                    array($lesson['id'], $insert_id));
            }
            $is_first = false;
        }

    }

    if ($price === '2500') {

        $cources = $db->select('select id from cources where id = 1');

        foreach ($cources as $cource) {
            $db->query('insert ignore into user_cources set id_user = {?}, id_cource = {?}',
                array($insert_id, $cource['id']));
        }

        $lessons = $db->select('select * from lessons where cource_id = 1 ORDER BY sort ASC');

        $i = 0;
        foreach ($lessons as $lesson) {

            if ($i >= 0 && $i < 2) {
                $db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="NEW" , pay = 1',
                    array($lesson['id'], $insert_id));
            }
            if ($i >= 2 && $i < 4) {
                $db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="CLOSE" , pay = 1',
                    array($lesson['id'], $insert_id));
            }
            if ($i >= 4) {
                $db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="CLOSE" , pay = 0',
                    array($lesson['id'], $insert_id));
            }

            $i++;
        }

    }

    $to      = $mail;
    $subject = 'Открытие курсов irs.academy';
    $message = 'Мы создали для Вас аккаунт на сайте <a href="https://irs.academy/">Онлайн курсы</a>. <br> Ваш логин: ' . $mail . '  <br> Ваш пароль : ' . $pass ;
    $message = wordwrap($message, 70, "\r\n");
    $headers = 'From: webmaster@irs.academy' . "\r\n" .
        'Reply-To: webmaster@irs.academy' . "\r\n" .
        'X-Mailer: PHP/' . phpversion() . "\r\n" ;
    $headers .= "Content-Type: text/html; charset=utf-8\r\n";

    var_dump(mail($to, $subject, $message, $headers));




}



class Db {

    private static $db = null;
    private $mysqli;
    private $sym_query = "{?}";

    public static function getDB($config) {
        if (self::$db == null) self::$db = new Db($config);
        return self::$db;
    }

    private function __construct($config) {
        $this->mysqli = new mysqli($config->host ,$config->user , $config->password, $config->dbname);
        $this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    private function getQuery($query, $params) {
        if ($params) {
            for ($i = 0; $i < count($params); $i++) {
                $pos = strpos($query, $this->sym_query);
                $arg = "'".$this->mysqli->real_escape_string($params[$i])."'";
                $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
            }
        }
        return $query;
    }

    public function select($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if (!$result_set) return  $this->mysqli->error;
        return $this->resultSetToArray($result_set);
    }

    public function query($query, $params = false) {
        $success = $this->mysqli->query($this->getQuery($query, $params));
        if ($success) {
            if ($this->mysqli->insert_id === 0) return true;
            else return $this->mysqli->insert_id;
        }
        else return $this->mysqli->error;
    }

    private function resultSetToArray($result_set) {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = $row;
        }
        return $array;
    }

    public function __destruct() {
        if ($this->mysqli) $this->mysqli->close();
    }

}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}