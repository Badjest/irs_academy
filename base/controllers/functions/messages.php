<?

function messages()
{
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin , id FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {
            header('Location: '.ROOT.'/setting');
            die;
        }
        $user_id = $result[0]['id'];
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $users = $db->select('select users.id, users.username, 
users.userlastname, 
count(Case messages.status when 0 THEN 1 else null end) as no_read 
from users left join messages on 
users.id = messages.user_id where users.id <> {?} AND users.admin = 0
GROUP by users.id ORDER BY no_read DESC', array($user_id));

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Сообщения',
            'username' => $username,
            'chat_on' => true
        )
    );


    echo loadView('templates/messages.php',
        array(
            'users' => $users,
            'id_user' => $user_id,
            'username' => $username
        ));

    echo loadView('templates/footer.php',array(
        'admin_ng' => true
    ));

}