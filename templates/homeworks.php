<? $data = $arrayOfData;?>
<div class="container" ng-controller="AdminHomeworks" ng-init="init(<?php echo htmlspecialchars(json_encode($data['homeworks'])); ?>)">


	<div class="col-md-12 col-sm-12 col-xs-12">
		

		
		<h3 class="def-title"></h3>


        <label>Поиск: <input ng-model="searchKeyword"></label>
			

		<div class="my-courses">
			
			<a ng-repeat="item in homeworks | filter: searchKeyword track by $index " href="<?=ROOT?>/homework/{{item.id_user_lesson}}" class="course-item clearfix">
				
				<div class="col-md-9 col-xs-9">
					<div><b>Имя</b> : {{item.username}} {{item.userlastname}}</div>
					<div><b>Дата:</b> {{item.last_hw}}</div>
					<div><b>Курс/урок: </b> {{item.courcename}} / {{item.name}}</div>
				</div>

				<div class="col-md-3 col-xs-3">
					<div ng-if="item.teacher_look == '0'" class="status tac">
						<span class="new">Не просмотрено</span>
					</div>
					<div ng-if="item.teacher_look == '1'" class="status tac">
						<span class="access">Просмотрено</span>
					</div>
				</div>

			</a>

		</div>

	</div>

</div>