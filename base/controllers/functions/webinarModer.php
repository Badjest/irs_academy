<?php

function webinarModer($url) {

    if (is_admin()) {

        $db = get_db();
        $webinar = $db->select('select * from webinars where url = {?}', array($url));
        if (count($webinar) == 0) {
            header('Location: '.ROOT.'/');
            die();
        }

        echo loadView('templates/header.php',
            array(
                'show_header'=>false,
                'title' => $webinar[0]['name'],
                'timer' => true
            )
        );

        $messages = $db->select('SELECT * FROM users_aggression where url = {?}
 ORDER by date desc', array($url));


        echo loadView('templates/webinarModer.php',array(
            'webinar' => $webinar[0],
            'messages' => $messages
        ));
        echo loadView('templates/footer.php',array(
            'admin_ng' => true
        ));
    }
}