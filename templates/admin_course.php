<? $data = $arrayOfData;?>
<div class="container" ng-controller="AdminCources" ng-init="init(<?php echo htmlspecialchars(json_encode($data['cources'])); ?>)">

	<a class="btn fly" href="<?=ROOT?>/admin_cource/new">Добавить<br>курс</a>

	<div class="col-md-12 col-sm-12 col-xs-12">
		

		
		<h3 class="def-title">Все курсы</h3>
			

		<div class="my-courses">
			
			<a ng-repeat="item in cources track by item.id" href="<?=ROOT?>/admin_cources/edit/{{item.id}}" class="course-item clearfix">
				
				<div class="col-md-10 col-xs-12">
                    <div class="col-md-3">
                        <img src="{{item.photo}}" alt="">
                    </div>
                    <div class="col-md-9">
                        <span>{{item.name}}</span>
                        <span>
						<div style="width: 120px; color:red;" ng-click="deleteCource($event, item.id)" href="">Удалить курс</div>
					</span>
                    </div>


				</div>

			</a>

		</div>

	</div>

</div>