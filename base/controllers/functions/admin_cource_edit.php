<?

function admin_cource_edit($id_cource) {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $cource = $db->select('Select * from cources where id = {?}', array($id_cource));
    $lessons = $db->select('Select * from lessons where cource_id = {?}', array($id_cource));
    $users = $db->select('select * from users');

    for ($i=0;$i < count($users); $i++) {
        foreach ($lessons as $lesson) {
            $result = $db->select('select id, stage from user_lessons where id_lesson = {?} AND id_user = {?}',
                array($lesson['id'],$users[$i]['id']));
            if (count($result) > 0) {
                $users[$i]['lessons'][] = array(
                    'id_user_lesson' => $result[0]['id'],
                    'name' => $lesson['name'],
                    'stage' => $result[0]['stage']
                );
            } else {
                $add = $db->query('insert ignore into user_lessons set id_lesson = {?}, id_user={?}, stage="CLOSE"',
                    array($lesson['id'], $users[$i]['id']));
                if (is_int($add)) {
                    $users[$i]['lessons'][] = array(
                        'id_user_lesson' => $add,
                        'name' => $lesson['name'],
                        'stage' => 'CLOSE'
                    );
                }

            }
        }
    }

    for($i=0;$i<count($users);$i++) {
        $count_us = $db->select('select id from user_cources where id_cource={?} and id_user={?}',
            array($id_cource,$users[$i]['id'] ));
        if (count($count_us) > 0) {
            $users[$i]['ok'] = 1;
        } else {
            $users[$i]['ok'] = 0;
        }
    }

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Редактировать '.$cource[0]['name'],
            'username' => $username
        )
    );
    echo loadView('templates/admin_course_edit.php',array(
        'cource'=> $cource[0],
        'lessons'=> $lessons,
        'users'=>$users
    ));
    echo loadView('templates/footer.php',array());
}


function admin_cource_new() {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $cource = array('name' => '');
    $lessons[0] = array(
        'name' => ''
    );
    $users = $db->select('select * from users');

    for($i=0;$i<count($users);$i++) {

        $users[$i]['ok'] = 0;

    }

    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Редактировать '.$cource[0]['name'],
            'username' => $username
        )
    );
    echo loadView('templates/admin_course_edit.php',array(
        'cource'=> $cource[0],
        'lessons'=> $lessons,
        'users'=>$users
    ));
    echo loadView('templates/footer.php',array());
}