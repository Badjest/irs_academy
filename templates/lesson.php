<? $data = $arrayOfData;?>

<? if ($data['stage'] != 'CLOSE') {?>
<div class="container" ng-controller="LessonController"
	 ng-init="init(<?php echo htmlspecialchars(json_encode($data['lesson'])); ?>,
<?php echo htmlspecialchars(json_encode($data['cource'])); ?>,
<?php echo htmlspecialchars(json_encode($data['user_lesson_id'])); ?>,
<?php echo htmlspecialchars(json_encode($data['ar_try'])); ?>,
'<?=$data['username']?>'
)">
	
	<div class="col-md-9 col-sm-9 col-xs-12">
		

		<div class="my-courses">
			
			<div class="course-item mini clearfix">
				
				<div class="col-md-12 col-xs-12">
					<img src="{{ cource.photo }}" alt="">
					<span>{{ cource.name }}</span>


					<h4>Пройдено уроков: {{cource.complete_lesson}} из {{cource.all_lesson}}</h4>
							<div class="project-process">
								<div class="line" style="width: {{cource.proc}}%";>
								</div>
				</div>
				</div>

				


			</div>

			<div class="lesson-area clearfix">
				
				<div class="top-area clearfix">
					<div class="col-md-8">
						<div class="title-of-lesson">
						<?/*?><span class="number-of-lesson">{{ lesson.sort }}</span><?*/?>
						<span>{{ lesson.name }}</span>
						</div>
					</div>
					<div class="col-md-4">

						<div class="right-side-item-g">
							Время выполнения:<br>
							<span  id='timeBack-l'>{{ lesson.timefor }}</span>
						</div>

					</div>
				</div>

				<div class="col-md-12">
					<div ng-repeat="item in lesson.video track by $index">
						<iframe  allowfullscreen="allowfullscreen"
								 mozallowfullscreen="mozallowfullscreen"
								 msallowfullscreen="msallowfullscreen"
								 oallowfullscreen="oallowfullscreen"
								 webkitallowfullscreen="webkitallowfullscreen"  src="" ng-src="{{item.path}}" frameborder="0" class="video-less"></iframe>
						<h2 style="text-align: center; margin-top:5px; margin-bottom: 45px; ">{{item.name}}</h2>
					</div>



				<div ng-bind-html="lesson.content" class="content clearfix">

				</div>
				</div>

				

			</div>

			<div class="warning-try">
            		<h3>Мои попытки</h3>
            		<div>
            			Уважаемые пользователи! У вас есть <b>две попытки</b> сдать домашнюю работу. Пожалуйста, будьте внимательны! 
						Для отправки файлов воспользуйтесь любым облачным хранилищем.
            		</div>
            	</div>

            <div ng-if="try.length > 0" class="my-try">

            	
                

               <div ng-repeat="item in try track by $index">
               	 <div class="try clearfix" ng-if="item.mark != '0'" >
               	    
               						<div class="col-md-6">
               							<div class="title-of-lesson">
               	    		<div>Результаты проверки домашнего задания</div>
               	        	<div>({{$index+1}} попытка)</div>
               	    	</div>
               						</div>
               	    
               	    <div class="col-md-4">
               							<div class="your-score-block">
               	       		Вы получили: <b>{{item.mark}} баллов</b>
               	    	</div>
               	    </div>
               						
               						<div class="col-md-2">
               							<div ng-if="item.mark > 25" class="status st-access">
               								<span class="access">Зачет</span>
               							</div>
               							<div ng-if="item.mark < 25" class="status">
               								<span class="fail">Не зачёт</span>
               							</div>
               						</div>
               	    
               	</div>
               	
               	            <div ng-if="item.mark === '0'" class="report-area clearfix">
               	
               					<div class="col-md-12">
               						<div class="title-of-lesson">
               							<span class="icon-report">
               							<img src="<?=BASE_LINK?>/img/rep.png"  alt="">
               							</span>
               							
               							<span>Задание на проверке</span>
               						</div>
               	
               						<div class="block-hm-prov">
               							<div>
               								Средняя продолжительность проверки: <b> 24 часа </b>
               							</div>
               							<div>
               								Следующий урок откроется после того, как домашнее задание будет проверено
               							</div>
               						</div>
               	
               						<div class="clock"></div>
               					
               	
               					</div>
               				</div>
               </div>

            </div>


			


			<? if ($data['send_files'] < 2) {?>
			<div class="report-area clearfix">

				<div class="col-md-12">
					<div class="title-of-lesson">
						<span class="icon-report">
						<img src="<?=BASE_LINK?>/img/rep.png"  alt="">
						</span>
						
						<span>Сдать домашнее задание</span>
					</div>
					
					

					<div class="block-hw">
						<textarea id="editor" ng-model="report" name="content">
						</textarea>

						<a href="" ng-click="sendCheck($event)" class="btn btn-send-hm">Отправить на проверку</a>
					</div>


					<? /*?>
					<input type="file" name="file" id="file" ng-model="files" onchange="angular.element(this).scope().uploadImage(this.files)" class="inputfile" />
					<label for="file">Прикрепить файл</label>


					<div class="files">
						<div ng-repeat="item in files track by $index">
						<span  class="file">
							<span class="file-name">
								{{ item.name }}
							</span>
							<span class="file-del" ng-click="delFile($index)">
								<button  type="button" class="close" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								Удалить
							</span>
						</span>
						</div>

					</div>
 				<? */?>


					

				</div>
				
				

			</div>
			<? } else { ?>
				<div class="warning-try">
					<h3>Внимание!</h3>
					<p>Вы исчерпали количество попыток по отправке домашних заданий для данного урока.</p>
				</div>

			<? } ?>


			

		</div>

	</div>

	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="col-md-12 npl npr">
			<h3 class="chat-title">Чат с преподавателем</h3>
		</div>
		</div>

	<div class="col-md-3 col-sm-3 col-xs-12">


		
		<div class="chatik clearfix">

			<div class="message-wrapp" >

				<?
				$messages = $data['messages'];

				foreach ($messages as $message) {
					if ($message['user_id'] === $data['cource']['id_user']) {
				?>

						<div class="message-to clearfix">
							<div class="col-xs-7 npr"><?=$message['username']?></div>
							<div class="col-xs-5 npl"><div class="date"><?=$message['date_send']?></div></div>
							<div class="col-xs-12">
								<div class="message-text message-text-to">
									<?=$message['message']?>
								</div>
							</div>

						</div>


				<? } else { ?>
						<div class="message-from clearfix">
							<div class="col-xs-5 npr"><div class="date"><?=$message['date_send']?></div></div>
							<div class="col-xs-7 npl"><span class="name_from"><?=$message['username']?></span></div>
							<div class="col-xs-12">
								<div class="message-text message-text-from">
									<?=$message['message']?>
								</div>
							</div>
						</div>
					<?}
				} ?>





			</div>
			
			<div class="col-xs-12">
				<form ng-submit="sendMessage($event)">
				<div class="message-area">
					<textarea class="textTo"  name="" id="" cols="30" rows="10" placeholder="Напишите сообщение..."></textarea>

					<input type="submit" class="btn btn-chat" value="Отправить">
				</div>
				</form>
			</div>

			

		</div>

	</div>

</div>
<? } else { ?>

	<div class="container">
		<h3>У вас нет доступа к этому уроку! Выполните домашнее задание предыдущего урока или свяжитесь с преподователем!</h3>
	</div>

<?}?>
