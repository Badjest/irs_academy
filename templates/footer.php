<? $data = $arrayOfData;?>


<!— Yandex.Metrika counter —>
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46826019 = new Ya.Metrika({
                    id:46826019,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46826019" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!— /Yandex.Metrika counter —>



<div class="bg-modal"></div>

<div class="modalBox">
	<h4 class="titleBox">Как бы вы хотели получать уведомления 
о выполненных домашних заданиях и новых сообщениях в чате?</h4>

	<div class="col-md-3 col-sm-12">
		
		<div class="notif-block">
			<input ng-model="notif" checked type="radio" name="notif" value="MAIL" id="mail-notif" />
								<label for="mail-notif">На почту</label>
		</div>

	</div>
	<div class="col-md-5 col-sm-12">
		<div class="notif-block">
			<input type="radio" ng-model="notif" name="notif" value="VK" id="vk-notif" />
								<label for="vk-notif">Личным сообщением в VK</label>
		</div>
	</div>
	<div class="col-md-4 col-sm-12">
		<div class="notif-block">
			<input type="radio" ng-model="notif" name="notif" value="MAIL_VK" id="vk-mail-notif" />
                <label for="vk-mail-notif">На почту и VK</label>
		</div>
	</div>

	<div class="col-md-12 col-sm-12">
		<div class="notif-block">
			<input ng-click="saveCheckNotif()" type="button" value="Подтвердить" class="btn modalB"/>
		</div>
	</div>

</div>






<script src="<?=BASE_LINK?>/libs/jquery/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>

<script src="<?=BASE_LINK?>/libs/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
<script src="<?=BASE_LINK?>/libs/angular/angular.min.js"></script>
<script src="<?=BASE_LINK?>/ng/angular-cookies.js"></script>

<?if (isset($data['admin_ng'])) {?>
    <script src="<?=BASE_LINK?>/ng/app-admin.js"></script>
<? } else {?>
    <script src="<?=BASE_LINK?>/ng/app.js"></script>
<?}?>
<script src="<?=BASE_LINK?>/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=BASE_LINK?>/libs/redactor/redactor/redactor.min.js"></script>
<script src="<?=BASE_LINK?>/libs/simplebar-2.4.4/dist/simplebar.js"></script>
<script src="<?=BASE_LINK?>/js/script.js"></script>

</body>

</html>

