<? $data = $arrayOfData;?>
<div class="container" ng-controller="LessonControllerAdmin"
	 ng-init="init(<?php echo htmlspecialchars(json_encode($data['lesson'])); ?>)">
	
	<div class="col-md-12 col-sm-12 col-xs-12">

		<a href="" ng-click="saveSet($event)" class="btn fly-save">Сохранить <br> изменения</a>

		<div class="my-courses">
			
			<div class="course-item clearfix">

				<img src="{{ lesson.photo }}" id="cource_photo" alt="">

				<label for="change_photo">Сменить фото</label>
				<input ng-model="file" ng-model="files" accept="image/x-png,image/gif,image/jpeg" onchange="angular.element(this).scope().uploadImage(this.files)" type="file" id="change_photo">
			</div>

			<div class="col-md-12 col-xs-12">
				<label for="phone">Название урока</label>
				<input type="text" ng-model="lesson.name" value="{{lesson.name}}" id="phone">
			</div>


			</div>

			<div class="lesson-area clearfix">
				


				<div class="col-md-12">

					<div>
					<label for="sort">Порядок урока (1,2,3..)</label>
					<input type="text" ng-model="lesson.sort" value="{{lesson.sort}}" id="sort">
					</div>

					<div>
						<label for="timefor">Продолжительность урока</label>
						<input type="text" ng-model="lesson.timefor" value="{{lesson.timefor}}" id="timefor">
					</div>

					<div style="border: 1px solid black;" ng-if="!item.del" ng-repeat="item in lesson.video track by $index">
						<label for="video-{{$index}}">id video c ютуба</label>
						<input type="text" ng-model="item.path" value="{{item.path}}" id="video-{{$index}}">
						<label for="video-name-{{$index}}">Имя видео</label>
						<input type="text" ng-model="item.name" value="{{item.name}}" id="video-name-{{$index}}">
						<label for="video-sort-{{$index}}">Порядок видео</label>
						<input type="text" ng-model="item.sort" value="{{item.sort}}" id="video-sort-{{$index}}">
						<div style="color: red;" ng-click="delVideo($index)">Удалить видео</div>
					</div>
					<button class="btn" ng-click="addVideo()" >Добавить видео</button>


				<div class="content clearfix">
					<label for="editor">Контент</label>

					<textarea id="editor" ng-model="lesson.content" name="content">
						{{ lesson.content }}
					</textarea>

				</div>
				</div>

                <input type="file" name="file" id="file" onchange="angular.element(this).scope().uploadFile(this.files)" class="inputfile" />
                <label for="file">Прикрепить файл</label>

				

			</div>




		</div>

	</div>



</div>