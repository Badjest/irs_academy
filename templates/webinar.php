<? $data = $arrayOfData;?>

<script src="https://irs.academy:8098/socket.io/socket.io.js"></script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-193300-4LOjp';</script>

<div ng-controller="WebinarController" ng-init="init(<?php echo htmlspecialchars(json_encode($data['webinar'])); ?>)">

    <header class="clearfix">

        <div class="col-md-6 col-xs-6">
            <h4 style="margin-top: 16px;" class="title_right_logo">
                Ведущий: <b><?=$data['webinar']['artist']?></b>
            </h4>
        </div>

        <div class="col-md-6 col-xs-6">
            <div class="right-side">
                <div class="nav-block hide-border">
                    <span id='yourName'>{{ user }}</span>
                    <span class="treug"></span>
                </div>
            </div>
        </div>

    </header>


    <?
    $openButtonCookie = ($_COOKIE['openButton_'.$data['webinar']['url']] === 'true' ? true : false );
    ?>



    <div class="" >

        <div class="col-md-9 col-sm-9 col-xs-12">


            <div class="lesson-area webinar clearfix">



                <div class="col-md-12">

                    <?
                    $is_fake = (int)$data['webinar']['fake'];


                    if (!$is_fake) { ?>
                        <style>
                            #video-webinar {
                                pointer-events: auto;
                                position: relative;
                            }
                            .webinar-wrapp {
                                position: relative;
                                height: auto;
                                overflow: auto;
                            }
                        </style>
                    <? }

                    $date = new DateTime();

                    $date->setTimezone(new DateTimeZone('Asia/Novosibirsk'));
                    $date = $date->format("H:i:s");


                    $openVideo = false;

                    if ($date >= $data['webinar']['start_video'] && $date <= $data['webinar']['end_video']) {
                        $openVideo = true;
                    }


                    ?>




                    <div <? if (!$openVideo && $is_fake) { echo 'style="display:none;"'; }?> class="webinar-wrapp">
                        <iframe id="video-webinar"
                                modestbranding="1"
                                allowfullscreen="allowfullscreen"
                                mozallowfullscreen="mozallowfullscreen"
                                msallowfullscreen="msallowfullscreen"
                                oallowfullscreen="oallowfullscreen"
                                webkitallowfullscreen="webkitallowfullscreen"
                                src="<?=$data['webinar']['video_url']?>"
                                frameborder="0" class="video-less">
                        </iframe>

                    </div>

                    <? if ($is_fake) { ?>
                    <div <? if (!$openVideo) { echo 'style="display:none;"'; }?> class="mobileNW">
                        Не воспроизводится трансляция?
                        <div class="btn" ng-click="mobileNW()">Исправить</div>
                    </div>
                    <? } ?>

                    <? if ($data['admin']) { ?>
                        <button ng-click="openVideoAndPlay()">open</button>
                        <button ng-click="hideVideoAndStop()">close</button>
                    <? } ?>

                    <? if ($data['admin']) { ?>
                        <button ng-click="openButtonsAdmin()">Открыть кнопки</button>
                        <button ng-click="closeButtonsAdmin()">Закрыть кнопки</button>
                    <? } ?>



                    <? if ($is_fake) { ?>
                    <img id="imgZagl" <? if ($openVideo) { echo 'style="display:none;"'; }?> src="<?=ROOT?>/img/zagl.jpg" alt="">
                    <? } ?>

                    <?

                    $open = false;

                    if (($date >= $data['webinar']['time_open_but'] && $date <= $data['webinar']['timer_close']) || $openButtonCookie) {
                        $open = true;
                    }

                    ?>


                    <div class="buttons-block" <? if(!$open) { echo 'style="display:none;"';} ?>>
                        <button onclick="yaCounter46826019.reachGoal('half'); return true;" ng-click="openModal('rassrochka')" class="btn modalB rassrochka">Рассрочка курса  "<?=$data['webinar']['name']?>"</button>

                        <div class="timer"></div>

                        <script>

                            <?

                            $time_explode = explode(':', $data['webinar']['time_start']);

                            ?>

                            $('.timer').syotimer({
                                hour: <?=(int)$time_explode[0]?> ,
                                minute: <?=(int)$time_explode[1]?> ,
                                lang: 'rus',
                                periodic: true,
                                periodInterval : 1,
                                layout: 'hms'
                            });
                        </script>

                        <button onclick="yaCounter46826019.reachGoal('full'); return true;"  ng-click="openModal('full')" class="btn samost">Оплатите и получите бонусы</button>

                    </div>


                </div>
            </div>

        </div>

        <div class="chat clearfix">
            <div ng-if="transNow" class="col-md-3 col-sm-3 col-xs-12">
                <div class="col-md-12 npl npr">
                    <div class="chat-title webinar">
                        <div class="counts">Участников: <span><?=$data['count_of_people']?></span></div>
                        <div class="names">
                            <? foreach ($data['names'] as $name) {?>
                                <div class="name">
                                    <?=$name['name'];?>
                                </div>
                            <? } ?>
                            <div class="name last">
                                + еще <?=((int)$data['count_of_people'] - count($data['names']))?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12">



                <div class="chatik clearfix">

                    <div class="message-wrapp rez">





                    </div>

                    <div class="col-xs-12">
                        <form ng-submit="sendMessage($event)">
                            <div class="message-area">
                                <textarea class="textTo" name="" id="" cols="30" rows="10" placeholder="Напишите сообщение..."></textarea>

                                <input type="submit" class="btn btn-chat" value="Отправить">
                            </div>
                        </form>
                    </div>



                </div>

            </div>

        </div>

        <div class="modalBox modalBox2">
            <h4 class="titleBox">Здравствуйте, пожалуйста, укажите свое имя</h4>

            <div class="col-md-12 col-sm-12">

                <div class="notif-block">
                    <input ng-model="user" placeholder="Мое имя" type="text" name="notif" id="mail-notif" />
                </div>

            </div>

            <div class="col-md-12 col-sm-12">
                <div class="notif-block">
                    <input type="button" ng-click="saveUser()" value="Подтвердить" class="btn modalB"/>
                </div>
            </div>

        </div>

        <div class="modalBox modalBox3">


            <form ng-submit="saveModal(); $event.preventDefault();" action="">


                <h4 class="titleBox"> {{modal.part}}
                    <br>
                    "<?=$data['webinar']['name']?>"
                </h4>

                <div class="col-md-3 col-sm-3">
                    <input type="hidden" value="<?=$_GET['utm_id']?>" id="utmid" style="display: none;">
                    <div class="notif-block">
                        <input ng-model="modal.name" placeholder="Ваше имя*" type="text" name="modal_name" id="mail-notif" />
                    </div>

                </div>
                <div class="col-md-3 col-sm-3">

                    <div class="notif-block">
                        <input ng-model="modal.last_name" required placeholder="Ваша фамилия *" type="text" name="notif" id="mail-notif" />
                    </div>

                </div>
                <div class="col-md-3 col-sm-3">

                    <div class="notif-block">
                        <input ng-model="modal.number"  placeholder="Номер телефона*" type="text" name="modal_number" id="mail-notif" />
                    </div>

                </div>
                <div class="col-md-3 col-sm-3">

                    <div class="notif-block">
                        <input ng-model="modal.email" placeholder="Ваш e-mail*" type="text" name="notif" id="mail-notif" />
                    </div>

                </div>

                <div class="col-md-12 col-sm-12">
                    <div class="notif-block">
                        <button type="submit" class="btn btn-access">Далее</button>
                    </div>
                </div>


            </form>


        </div>

    </div>

</div>

<? if ($data['admin']) { ?>
    <div class="container">



        <div class="admin" ng-controller="WebinarAdminController"
             ng-init="init(<?php echo htmlspecialchars(json_encode($data['webinar'])); ?> ,
             <?php echo htmlspecialchars(json_encode($data['messages'])); ?>)">


            <h1>Все время новосибирское</h1>

            <a href="" ng-click="saveSet($event)" class="btn fly-save">Сохранить</a>
            <a href="" ng-click="addMessage($event)" class="btn fly">Добавить<br>сообщение</a>
            <h1>Админ панель</h1>

            <div>
                <label for="name">Название вебинара</label>
                <input type="text" ng-model="webinar.name" value="{{webinar.name}}" id="name">
            </div>

            <div>
                <label for="artist">Ведущий</label>
                <input type="text" ng-model="webinar.artist" value="{{webinar.artist}}" id="artist">
            </div>


            <div>
                <label for="vidos">Видос</label>
                <input type="text" ng-model="webinar.video_url" value="{{webinar.video_url}}" id="vidos">
            </div>


            <div>
                <label for="time">Время таймера</label>
                <input type="datetime" ng-model="webinar.time_start" value="{{webinar.time_start}}" id="time">
            </div>


            <div>
                <label for="timeOpen">Время появления кнопок</label>
                <input type="datetime" ng-model="webinar.time_open_but" value="{{webinar.time_open_but}}" id="timeOpen">
            </div>

            <div>
                <label for="timeClose">Время исчезания кнопок</label>
                <input type="datetime" ng-model="webinar.timer_close" value="{{webinar.timer_close}}" id="timeClose">
            </div>

            <div>
                <label for="timeStartVideo">Время включения видоса</label>
                <input type="datetime" ng-model="webinar.start_video" value="{{webinar.start_video}}" id="timeStartVideo">
            </div>

            <div>
                <label for="timeEndVideo">Время выключения видоса</label>
                <input type="datetime" ng-model="webinar.end_video" value="{{webinar.end_video}}" id="timeEndVideo">
            </div>


            <h1>Сообщения</h1>

            <div class="messages">

                <div class="clearfix" style="border: 1px solid black; margin: 5px;"
                     ng-repeat="item in messages track by $index">

                    <div class="col-xs-6">



                        <div>
                            <label for="m_name-{{item.id}}">Имя</label>
                            <input type="text" ng-model="item.name"
                                   value="{{item.name}}" id="m_name-{{item.id}}">
                        </div>

                    </div>
                    <div class="col-xs-6">

                        <div>
                            <label for="m_time-{{item.id}}">Время написания</label>
                            <input type="text" ng-model="item.time"
                                   value="{{item.time}}" id="m_time-{{item.id}}">
                        </div>

                    </div>

                    <div class="col-xs-12">

                        <div>
                            <label for="m_message-{{item.id}}">Сообщение</label>
                            <input type="text" ng-model="item.message"
                                   value="{{item.message}}" id="m_message-{{item.id}}">
                        </div>

                    </div>

                    <div class="col-xs-12">
                        <div>
                            <a style="width: 120px; color:red;" ng-click="delMessage($index)" href="">Удалить</a>
                        </div>
                    </div>


                </div>


            </div>




        </div>

    </div>

<? } ?>




<form style="display: none;" id="payForm" name="ShopForm" action="https://money.yandex.ru/eshop.xml" method="post">

    <input name="ShopID" value="" type="hidden">
    <input name="scid" value="" type="hidden">
    <input name="customerNumber" value="" type="hidden">
    <input name="orderNumber" value="" type="hidden">
    <input name="Sum" value="" type="hidden">
    <input name="paymentType" value="" type="hidden">
    <input name="cms_name" value="" type="hidden">
    <input name="BX_HANDLER" value="" type="hidden">
    <input name="BX_PAYSYSTEM_CODE" value="" type="hidden">

    <div class="sale-paysystem-yandex-button-container">
			<span class="sale-paysystem-yandex-button">
				<input class="sale-paysystem-yandex-button-item" name="BuyButton" value="Оплатить" type="submit">
			</span><!--sale-paysystem-yandex-button-->
    </div><!--sale-paysystem-yandex-button-container-->

</form>



<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>

