<? $data = $arrayOfData;?>
<div class="container" ng-controller="HomeController"
	 ng-init="init(<?php echo htmlspecialchars(json_encode($data['homeworks'])); ?>,
	 <?php echo htmlspecialchars(json_encode($data['homework_status'])); ?>,
	 <?php echo htmlspecialchars(json_encode($data['other_lessons'])); ?>,
	 <?php echo htmlspecialchars(json_encode($data['id_room'])); ?>,
	 <?php echo htmlspecialchars($data['id_user']); ?>,
	 <?php echo htmlspecialchars(json_encode($data['info'])); ?>)">
	
	<div class="col-md-8 col-sm-8 col-xs-12">

		<div class="col-md-12">

		<div class="my-courses">
			
			<div class="course-item clearfix">
				
				<div class="col-md-12 col-xs-12">
                    <div class="col-md-6 col-xs-12">
                        <span style="padding-top: 35px; display: block;">{{ info.courcename }}</span>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div style="margin-top: 30px;" ng-if="info.teacher_look == '0'" class="status hw tac">
                            <span class="new">Не просмотрено</span>
                        </div>
                        <div style="margin-top: 30px;" ng-if="info.teacher_look == '1'" class="status hw tac">
                            <span class="access">Просмотрено</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <button class="btn btn-auth no-read-btn" ng-click="noRead(info.id_user_lesson)">Оставить <br> непрочитанным</button>
                    </div>


				</div>


			</div>

			<div class="lesson-area clearfix">
				
				<div class="top-area clearfix">
					<div class="col-md-6">
						<div class="title-of-lesson">
<!--						<span class="number-of-lesson">0</span>-->
						<span>{{ info.name }}</span>
						</div>
					</div>
					<div class="col-md-6">

					</div>
				</div>



				<h1>Отчеты пользователя {{info.username}} {{info.userlastname}}</h1>

				<div ng-repeat="item in homeworks track by $index" class="try clearfix">

					<div class="title-of-lesson">
						<span>Попытка {{$index+1}}</span>
					</div>

					<div>
						<h3>Отчет</h3>

						<div ng-bind-html="item.content">

						</div>
					</div>

					<div class="report-area clearfix">
						<h2>Прикрепленные файлы к попытке {{$index+1}}</h2>
						<div class="files">
							<div ng-repeat="file in item.files">
						<span  class="file">
							<a href="{{file.path}}" class="file-name">
								{{ file.filename }}
							</a>
						</span>
							</div>

						</div>
					</div>

					<div class="col-md-12 col-xs-12">
						<label for="mark-{{$index}}">Оценка за попытку (0 - 100 баллов)</label>
						<input type="text" ng-model="item.mark" value="{{item.mark}}" id="mark-{{$index}}">
						<button class="btn btn-auth" ng-click="saveMark(item)">Оценить</button>
					</div>

				</div>


				

			</div>






			<div>
				<h3 style="margin-left: 10px;"><b>Данный урок для пользователя:</b></h3>

					<div class="clearfix">
						<div class="col-md-12">
							<input type="radio" ng-model="homework_status.stage" value="NEW" id="inp-lesson-new-{{homework_status.id}}" />
							<label for="inp-lesson-new-{{homework_status.id}}">Открыт</label>

							<input type="radio" ng-model="homework_status.stage" value="CLOSE" id="inp-lesson-close-{{homework_status.id}}" />
							<label for="inp-lesson-close-{{homework_status.id}}">Доступ закрыт</label>

							<input type="radio" ng-model="homework_status.stage" value="ACCESS" id="inp-lesson-access-{{homework_status.id}}" />
							<label for="inp-lesson-access-{{homework_status.id}}">Выполнен</label>

							<input type="radio" ng-model="homework_status.stage" value="FAIL" id="inp-lesson-fail-{{homework_status.id}}" />
							<label for="inp-lesson-fail-{{homework_status.id}}">НЕ зачет</label>
						</div>
					</div>

			</div>

			<div>
				<h3 style="margin-left: 10px;"><b>Другие уроки курса для пользователя:</b></h3>

					<div class="clearfix">
						<div class="col-md-12" ng-repeat="less in other_lessons | orderBy:'sort' track by less.id">
							<h4>{{less.name}}</h4>
							<input ng-disabled="less.pay == '0'" type="radio" ng-model="less.stage" value="NEW" id="inp-lesson-new-{{less.id}}" />
							<label for="inp-lesson-new-{{less.id}}">Открыт</label>

							<input ng-disabled="less.pay == '0'" type="radio" ng-model="less.stage" value="CLOSE" id="inp-lesson-close-{{less.id_user_lesson}}" />
							<label for="inp-lesson-close-{{less.id}}">Доступ закрыт</label>

							<input ng-disabled="less.pay == '0'" type="radio" ng-model="less.stage" value="ACCESS" id="inp-lesson-access-{{less.id_user_lesson}}" />
							<label for="inp-lesson-access-{{less.id}}">Выполнен</label>
						</div>
					</div>

			</div>


			<div class="btn btn-access" ng-click="saveSet()">Сохранить настройки</div>
				

			</div>

			</div>

		</div>


	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="col-md-12">
			<h3>Чат с учеником</h3>
		</div>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">


		<div class="chatik clearfix">

			<div class="message-wrapp" >

				<?
				$messages = $data['messages'];

				foreach ($messages as $message) {
					if ($message['user_id'] === $data['id_user']) {
						?>

						<div class="message-to clearfix">
							<div class="col-xs-7 npr"><?=$message['username']?></div>
							<div class="col-xs-5 npl"><div class="date"><?=$message['date_send']?></div></div>
							<div class="col-xs-12">
								<div class="message-text message-text-to">

                                        <?=$message['message']?>

								</div>
							</div>

						</div>


					<? } else { ?>
						<div class="message-from clearfix">
							<div class="col-xs-5 npr"><div class="date"><?=$message['date_send']?></div></div>
							<div class="col-xs-7 npl"><span class="name_from"><?=$message['username']?></span></div>
							<div class="col-xs-12">
								<div class="message-text message-text-from">

									<?=$message['message']?>
								</div>
							</div>
						</div>
					<?}
				} ?>





			</div>

			<div class="col-xs-12">
				<form ng-submit="sendMessage($event)">
					<div class="message-area">
						<textarea class="textTo h300" title="Отправить(ctr+enter)" name="" id="" cols="30" rows="10" placeholder="Напишите сообщение... Отправить (ctr+enter)"></textarea>

						<input type="submit" class="btn btn-auth" value="Отправить">
					</div>
				</form>
			</div>



		</div>

	</div>

		

	</div>



</div>