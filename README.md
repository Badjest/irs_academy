irs.academy

# Языки программирования : 
* **PHP** 
* **JavaScript**
# База данных:  
* **MySql**
 

# Описание #
 Ядро находится в папке base. Все маршруты прописаны в файле routing.php,
 в папке controllers находятся основные функции, вызываемые routing-ом.
 В папке chat находится чатик, он работает на node.js + express.js + socket.io.