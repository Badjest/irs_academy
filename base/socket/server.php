<?php
date_default_timezone_set('Asia/Novosibirsk');
$config_db = include "../config/config_db.php";


set_time_limit(0);

$host = 'localhost'; //host
$port = '8090'; //port
$null = NULL; //null var



if(!($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);

    die("Couldn't create socket: [$errorcode] $errormsg \n");
}

echo "Socket created \n";

//reuseable port

if(!socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);

    die("Couldn't set option to socket socket: [$errorcode] $errormsg \n");
}

echo "Set options to socket \n";



//bind socket to specified host

if(!socket_bind($socket, 0, $port))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);

    die("Couldn't socket_bind: [$errorcode] $errormsg \n");
}

echo "Success socket_bind \n";

//listen to port


if(!socket_listen($socket))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);

    die("Couldn't socket_bind: [$errorcode] $errormsg \n");
}

echo "Socket listen! \n";


//create & add listning socket to the list
$clients = array(
    array('socket'=>$socket,
        'chatroom' => 0)
);



//start endless loop, so that our script doesn't stop
while (true) {
    //manage multipal connections

    $changed = array();

    foreach ($clients as $value) {
        $changed[] = $value['socket'];
    }




    //returns the socket resources in $changed array
    socket_select($changed, $null, $null, 0, 10);

    //check for new socket
    if (in_array($socket, $changed)) {
        $socket_new = socket_accept($socket); //accpet new socket

        //var_dump($socket_new);

        $clients[]['socket'] = $socket_new; //add socket to client array

        $header = socket_read($socket_new, 1024); //read data sent by the socket
        perform_handshaking($header, $socket_new, $host, $port); //perform websocket handshake



        socket_getpeername($socket_new, $ip); //get ip address of connected socket
        $response = mask(json_encode(array('type'=>'system', 'message'=>$ip.' connected'))); //prepare json data
        send_message($response); //notify all users about new connection

        //make room for new socket
        $found_socket = array_search($socket, $changed);
        unset($changed[$found_socket]);
    }

    //loop through all connected sockets
    foreach ($changed as $changed_socket) {

        //check for any incomming data

        while(socket_recv($changed_socket, $buf, 1024, 0) >= 1)
        {
            $received_text = unmask($buf); //unmask data
            $tst_msg = json_decode($received_text); //json decode
            $event = '';
            if (isset($tst_msg->event)) {
                $event = $tst_msg->event;
            }



            if ($event === 'set_chatroom') {
                $chat_room = $tst_msg->chatroom;
                for ($i = 0; $i < count($clients) ; $i++) {
                    if ($changed_socket == $clients[$i]['socket']) {
                        $clients[$i]['chatroom'] =  $chat_room;
                        break;
                    }
                }
            }

            if ($event === 'send_message') {
                $chat_room = $tst_msg->chatroom;
                $user_message = $tst_msg->message; //message text
                $id_user = $tst_msg->id;
                $db = Db::getDB($config_db);

                $result = $db->select('select username from users where id = {?}',array($id_user));
                $user_name = $result[0]['username'];
                //var_dump($result);
                $date = date("Y-m-d H:i:s");

                // записываем сообщение в бд
                $shifr_mes = base64_encode($user_message);
                $res = $db->query('insert into messages set date_send = {?}, status = 0 , message={?}, id_user_lesson = {?}, user_id = {?}',
                    array($date,$shifr_mes, $chat_room, $id_user));



                //prepare data to be sent to client
                $response_text = mask(json_encode(array('type'=>'usermsg',
                    'name'=>$user_name,
                    'message'=>$user_message,
                    'id'=>$id_user,
                    'date'=> $date)));
                send_message($response_text, $chat_room); //send data
            }


            break 2; //exist this loop
        }


        $buf = @socket_read($changed_socket, 1024, PHP_NORMAL_READ);
        if ($buf === false) {
            for ($i = 0; $i < count($clients) ; $i++) {
                if ($changed_socket == $clients[$i]['socket']) {
                    $flag = $i;
                    break;
                }
            }
            socket_getpeername($changed_socket, $ip);
            array_splice($clients, $flag, 1);


            //notify all users about disconnected connection
            $response = mask(json_encode(array('type'=>'system', 'message'=>$ip.' disconnected')));
            send_message($response);
        }
    }
}
// close the listening socket
socket_close($socket);

function send_message($msg, $id_room = 0)
{
    global $clients;
    foreach($clients as $changed_socket)
    {
        if ($id_room <> 0) {
            if ($changed_socket['chatroom'] === $id_room || $changed_socket['chatroom'] === 'admin_hhssch') {
                @socket_write($changed_socket['socket'],$msg,strlen($msg));
            }
        } else {
            @socket_write($changed_socket['socket'],$msg,strlen($msg));
        }

    }
    return true;
}


//Unmask incoming framed message
function unmask($text) {
    $length = ord($text[1]) & 127;
    if($length == 126) {
        $masks = substr($text, 4, 4);
        $data = substr($text, 8);
    }
    elseif($length == 127) {
        $masks = substr($text, 10, 4);
        $data = substr($text, 14);
    }
    else {
        $masks = substr($text, 2, 4);
        $data = substr($text, 6);
    }
    $text = "";
    for ($i = 0; $i < strlen($data); ++$i) {
        $text .= $data[$i] ^ $masks[$i%4];
    }
    return $text;
}

//Encode message for transfer to client.
function mask($text)
{
    $b1 = 0x80 | (0x1 & 0x0f);
    $length = strlen($text);

    if($length <= 125)
        $header = pack('CC', $b1, $length);
    elseif($length > 125 && $length < 65536)
        $header = pack('CCn', $b1, 126, $length);
    elseif($length >= 65536)
        $header = pack('CCNN', $b1, 127, $length);
    return $header.$text;
}

//handshake new client.
function perform_handshaking($receved_header,$client_conn, $host, $port)
{
    $headers = array();
    $lines = preg_split("/\r\n/", $receved_header);
    foreach($lines as $line)
    {
        $line = chop($line);
        if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
        {
            $headers[$matches[1]] = $matches[2];
        }
    }


    $secKey = $headers['Sec-WebSocket-Key'];
    $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
    //hand shaking header
    $upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
        "Upgrade: websocket\r\n" .
        "Connection: Upgrade\r\n" .
        "WebSocket-Origin: $host\r\n" .
        "WebSocket-Location: wss://$host:$port/demo/shout.php\r\n".
        "Sec-WebSocket-Accept:$secAccept\r\n\r\n";
    socket_write($client_conn,$upgrade,strlen($upgrade));
}

class Db {

    private static $db = null;
    private $mysqli;
    private $sym_query = "{?}";

    public static function getDB($config) {
        if (self::$db == null) self::$db = new Db($config);
        return self::$db;
    }

    private function __construct($config) {
        $this->mysqli = new mysqli($config->host ,$config->user , $config->password, $config->dbname);
        $this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    private function getQuery($query, $params) {
        if ($params) {
            for ($i = 0; $i < count($params); $i++) {
                $pos = strpos($query, $this->sym_query);
                $arg = "'".$this->mysqli->real_escape_string($params[$i])."'";
                $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
            }
        }
        return $query;
    }

    public function select($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if (!$result_set) return  $this->mysqli->error;
        return $this->resultSetToArray($result_set);
    }

    public function query($query, $params = false) {
        $success = $this->mysqli->query($this->getQuery($query, $params));
        if ($success) {
            if ($this->mysqli->insert_id === 0) return true;
            else return $this->mysqli->insert_id;
        }
        else return $this->mysqli->error;
    }

    private function resultSetToArray($result_set) {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = $row;
        }
        return $array;
    }

    public function __destruct() {
        if ($this->mysqli) $this->mysqli->close();
    }

}

