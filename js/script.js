$(document).ready(function() {

	$('#editor').redactor();


	$('.user-message-item').on('click', function () {
		$('.user-message-item').removeClass('active');
		$('.user-lesson-message-item').removeClass('active');
		$(this).addClass('active');
	});

	$(document).on('click', '.user-lesson-message-item', function () {
		$('.user-lesson-message-item').removeClass('active');
		$(this).addClass('active');
	});

    $('input[name="modal_number"]').mask("+9 (999) 999 99 99");

});