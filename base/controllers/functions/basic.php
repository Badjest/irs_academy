<?

/**
 * Базовые функции
 * */

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function is_admin() {
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }
    return true;
}

function get_db() {
    require_once 'base/classes/DB/Db.php';
    $config_db = include "base/config/config_db.php";
    return Db::getDB($config_db);
}



function echo_error($message) {
    echo json_encode(array(
        'error' => true,
        'message' => $message
    ));
}

function echo_success($message) {
    echo json_encode(array(
        'success' => true,
        'message' => $message
    ));
}

function loadView ($strViewPath, $arrayOfData)
{
    extract($arrayOfData);
    ob_start();
    require($strViewPath);

    $strView = ob_get_contents();
    ob_end_clean();
    return $strView;
}


function getDateNow() {
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('Asia/Novosibirsk'));
    $date = $date->format("H:i:s");
    echo $date;
}

/**
 * Функция фиксирует дату входа пользователя по hash
 */
function update_last_use() {
    $hash = $_COOKIE['hash'];
    $db = get_db();
    if ($hash) {
        $db->query('update users set last_use = NOW() where hash={?}', array($hash));
    }


}