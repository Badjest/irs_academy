<?
/**
 * Функция сохранения курса
 * */
function saveCource() {

    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        return;
    }

    $filename = '';
    foreach ($_FILES as $item) {
        $uploaddir = '/academy'.ROOT.'/uploads/';
        $filename = generateRandomString(7). basename($item['name']);
        $uploadfile = $uploaddir . $filename;
        move_uploaded_file($item['tmp_name'], $uploadfile);
    }

    $cource_name = $_POST['cource_name'];
    $lessons = json_decode($_POST['lessons']);
    $users = json_decode($_POST['users']);
    $id_cource = $_POST['id_cource'];



    if ($id_cource <> 'undefined') {
        if ($filename <> '') {
            $db->query('update cources set cources.name={?} , photo={?} where id = {?}',
                array($cource_name, ROOT.'/uploads/'.$filename,$id_cource));
        } else {
            $db->query('update cources set cources.name={?} where id = {?}',
                array($cource_name,$id_cource));
        }
    } else {
        $id_cource = $db->query('insert ignore into cources set cources.name={?} , photo={?}',
            array($cource_name, ROOT.'/uploads/'.$filename));
    }

    foreach ($lessons as $item) {
        if (isset($item->id)) {
            $db->query('update lessons set name={?}  where id = {?}',
                array($item->name, $item->id));
        } else {
            $db->query('insert ignore into lessons set name={?}, cource_id={?}',
                array($item->name, $id_cource));
        }
    }

    /*foreach ($users as $item) {

        // уроки для пользователей (доступ)
        foreach ($item->lessons as $lesson) {
            $db->query('update user_lessons set stage = {?} where id = {?}',array($lesson->stage, $lesson->id_user_lesson));
        }

        if ($item->ok === 1) {
            $count = $db->select('select id from user_cources where id_user = {?} and id_cource = {?}',
                array($item->id, $id_cource));
            if (count($count) > 0) {
                continue;
            } else {
                $db->query('insert ignore into user_cources set id_cource={?}, id_user={?}',
                    array($id_cource, $item->id));
            }
        } else {
            $id = $db->select('select id from user_cources where id_user = {?} and id_cource = {?}',
                array($item->id, $id_cource));
            if (count($id)> 0) {
                $db->query('delete from user_cources where id = {?}', array($id[0]['id']));
            }
        }



    }*/
    echo_success($id_cource);
}


/**
 * Функция сохранения статуса урока
 * */
function saveStatusLesson() {
    $data = $_POST;
    $user_lesson_this = $data['homework'];
    $other_lessons = $data['other_lessons'];
    $db = get_db();
    foreach ($other_lessons as $lesson) {
        $db->query('update user_lessons set stage = {?} where id = {?}',array($lesson['stage'], $lesson['id']));
    }
    $return = $db->query('update user_lessons set stage = {?} where id = {?}',array($user_lesson_this['stage'], $user_lesson_this['id']));

    if (is_bool($return) and $return) {
        echo_success('ok');
    } else {
        echo_error($return);
    }
}


/**
 * Функция удаление курса
 * */
function deleteCource() {


    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        return;
    }

    $data = $_POST;
    $id_cource = $data['id_cource'];
    $result = $db->query('delete from user_cources where id_cource={?}', array($id_cource));
    if (is_string($result)) {
        echo_error('Не удалось удалить user_cources: ' . $result);
        return;
    }
    $lessons = $db->select('select id from lessons where cource_id = {?}', array($id_cource));
    $string = '';
    foreach ($lessons as $lesson) {
        $string .= ' id_lesson = ' . $lesson['id'] . ' OR ';
        $string_us = ' id = ' .$lesson['id'] . ' AND ';
    }
    $string = substr($string, 0, -3);
    $string_us = substr($string_us, 0, -4);
    $result = $db->query('delete from user_lessons where ' . $string);

    if (is_string($result)) {
        echo_error('Не удалось удалить user_lessons: ' . $result);
        return;
    }

    $result = $db->query('delete from lessons where ' . $string_us);

    if (is_string($result)) {
        echo_error('Не удалось удалить lessons: ' . $result);
        return;
    }

    $result = $db->query('delete from cources where id = {?}', array($id_cource));

    if (is_string($result)) {
        echo_error('Не удалось удалить ciurces: ' . $result);
        return;
    }

    echo_success('Удалено!');

}

/**
 * Функция удаления урока
 * */
function deleteLesson($id_lesson) {


    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Нет доступа');
        return;
    }

    $all_homeworks = $db->select('select id from homeworks where id_user_lesson = {?}',
        array($id_lesson));


    // удаляем весь контент
    foreach ($all_homeworks as $hw) {
        $db->query('delete from users_content where 	id_homework = {?}', array($hw['id']) );
    }

    // удаляем все домашки
    $db->query('delete from homeworks where id_user_lesson = {?}', array($id_lesson) );

    // удаляем все видосы
    $db->query('delete from lesson_videos where id_lesson = {?}', array($id_lesson) );

    // удаляем все сообщения
    $db->query('delete from messages where id_user_lesson = {?}', array($id_lesson) );

    // удаляем все сообщения
    $db->query('delete from messages where id_user_lesson = {?}', array($id_lesson) );

    // удаляем user_lessons
    $db->query('delete from user_lessons where id_lesson = {?}', array($id_lesson) );

    // удаляем урок
    $db->query('delete from lessons where id = {?}', array($id_lesson) );


    echo_success('Удалено!');
}


/**
 * Функция сохранения урока с файлами
 * */
function saveLesson() {

    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        return;
    }

    $filename = '';
    foreach ($_FILES as $item) {
        $uploaddir = '/academy/'.ROOT.'/uploads/';
        $filename = generateRandomString(7). basename($item['name']);
        $uploadfile = $uploaddir . $filename;
        move_uploaded_file($item['tmp_name'], $uploadfile);
    }

    $lesson = json_decode($_POST['lesson']);
    $videos = $lesson->video;
    foreach ($videos as $video) {

        if (isset($video->del) && isset($video->id)) {
            $db->query('delete from lesson_videos where id = {?}',array($video->id));
            continue;
        }
        if (isset($video->id)) {
            $db->query('update lesson_videos set lesson_videos.name = {?},  path = {?}, sort = {?} where id = {?}',
                array($video->name,$video->path,$video->sort,$video->id));
            continue;
        }
        if (!isset($video->del)) {
            $db->query('insert ignore into lesson_videos set lesson_videos.name = {?} ,  path = {?} , id_lesson = {?}, sort = {?}',
                array($video->name,$video->path,$lesson->id, $video->sort));
        }

    }


    if ($filename <> '') {
        $result = $db->query('update lessons set 
                                        lessons.name = {?}, 
                                        content = {?}, 
                                        photo = {?}, 
                                        timefor ={?}, 
                                        sort = {?}
                                        WHERE id = {?}',

            array(
                $lesson->name,
                $lesson->content,
                ROOT.'/uploads/'.$filename,
                $lesson->timefor,
                $lesson->sort,
                $lesson->id
            )
        );
    } else {
        $result = $db->query('update lessons set 
                                        lessons.name = {?}, 
                                        content = {?}, 
                                        timefor ={?}, 
                                        sort = {?}
                                        WHERE id = {?}
                                        ',

            array(
                $lesson->name,
                $lesson->content,
                $lesson->timefor,
                $lesson->sort,
                $lesson->id
            )
        );
    }

    if ($result) {
        echo_success('ok');
    } else {
        echo_error($result);
    }

}


/**
 * Функция проверки доступа у уроку
 * */
function access_cource($id_user, $id_cource) {
    $db = get_db();
    $result = $db->select('Select id from user_cources where id_cource={?} AND id_user={?}',
        array($id_cource,$id_user));

    //var_dump($id_user, $id_cource);

    if (is_array($result) && count($result) == 0 ) {
        header('Location: '.ROOT.'/my');
        die;
    }

}


/**
 * Функция логина
 * */
function login() {
    $db = get_db();
    $data = $_POST;
    $result = $db->select('SELECT * FROM users where LOWER(mail) = {?} and pass = {?} ', array(strtolower($data['email']), $data['password']));
    if (is_array($result) && count($result) > 0) {
        $hash = hash('ripemd160',  $result[0]['mail'] . $result[0]['pass']);
        $db->query('update users set hash={?} where id={?}',array($hash, $result[0]['id']));
        setcookie("hash", $hash, time()+3600000);
        echo_success('ok');
    } else {
        echo_error("Логин или пароль неверны");
    }
}


/**
 * Функция сохранения пользователя
 * */
function saveUser() {

    $db = get_db();
    $data = $_POST;
    $hash = $_COOKIE['hash'];
    if (isset($data['pass'])) {
        $result = $db->query('UPDATE users SET mail={?} , username={?}, userlastname={?}, phone={?}, vk={?}, notifdz={?},
      notifmes={?}, notifnews={?}, notifend={?}, notifreport={?} , pass={?} , vk_notif = {?}, mail_notif = {?} WHERE hash = {?} ',
            array($data['mail'], $data['username'], $data['userlastname'], $data['phone'], $data['vk'],
                $data['notifdz'],$data['notifmes'],$data['notifnews'],$data['notifend'],$data['notifreport'],$data['pass'],
                $data['vk_notif'],$data['mail_notif'], $hash));
    } else {
        $result = $db->query('UPDATE users SET mail={?} , username={?}, userlastname={?}, phone={?}, vk={?}, notifdz={?},
      notifmes={?}, notifnews={?}, notifend={?}, notifreport={?} , vk_notif = {?}, mail_notif = {?} WHERE hash = {?} ',
            array($data['mail'], $data['username'], $data['userlastname'], $data['phone'], $data['vk'],
                $data['notifdz'],$data['notifmes'],$data['notifnews'],$data['notifend'],$data['notifreport'],
                $data['vk_notif'],$data['mail_notif'],$hash));
    }

    if (is_bool($result)) {
        echo_success('ok');
    } else {
        echo_error($result);
    }
}


/**
 * Функция массового сохранения пользователей
 * */
function saveUsers() {
    $db = get_db();
    $data = $_POST;


    foreach ($data['users'] as $item) {


        if (isset($item['id'])) {
            $db->query('update users set username = {?} ,
                        mail = {?}, pass = {?}, phone = {?}, userlastname = {?},
                        admin = '.(int)$item['admin'].' where id = {?}',
                array($item['username'],$item['mail'], $item['pass'],
                    $item['phone'], $item['userlastname'], $item['id']));

        } else {

            $db->query('insert ignore into users set username = {?} ,
                        mail = {?}, pass = {?}, phone = {?}, userlastname= {?},
                        admin = {?}',
                array($item['username'],$item['mail'], $item['pass'],
                    $item['phone'], $item['userlastname'], (int)$item['admin'] ));

        }
    }

    echo_success('ok');
}


/**
 * Функция восстановления пароля
 * */
function recoverPass() {

    $db = get_db();
    $data = $_POST;

    if (isset($data['mail'])) {
        $result = $db->select('select id from users where mail = {?}',array($data['mail']));
        if (is_array($result) && count($result) == 1) {
            $new_pass = generateRandomString(8);
            $message = 'Ваш новый пароль: ' .$new_pass;
            $mail_process = mail($data['mail'], 'Восстановление пароля',$message,"From: Онлайн курсы ИРС" . "\r\n" . "Reply-To: admin@ирсиб.рф" . "\r\n" . "X-Mailer: PHP/" . phpversion() . "\r\n" . "Content-type: text/html; charset=\"utf-8\"");
            if ($mail_process) {
                $db->query('update users set pass = {?} where mail = {?}', array($new_pass,$data['mail']));
                echo_success('ok');
            }
        } else {
            echo_error('Пользователя с данным Email адресом не найдено');
        }
    } else {
        echo_error('Данные не получены');
    }

}


/**
 * Функция сохранения данных по домашним заданиям
 * */
function sendData() {

    $db = get_db();
    $data = $_POST;
    $content = $data['content'];
    $id_user_lesson = $data['id_user_lesson'];

    $update_count_try = $db->query('Update user_lessons set send_files = send_files + 1 where id = {?}',
        array($id_user_lesson));

    if (is_bool($update_count_try) && $update_count_try) {
        $count_try = $db->select('Select send_files from user_lessons where id = {?}',
            array($id_user_lesson));
        $count_try = $count_try[0]['send_files'];

        $homework_id = $db->query('insert into homeworks set content = {?} , id_user_lesson = {?} , try = {?}',
            array($content, $id_user_lesson, $count_try));


        /*$filenames = array();

        foreach ($_FILES as $item) {
            $uploaddir = '/academy/uploads/';
            $filename = generateRandomString(7). basename($item['name']);
            $filenames[] = $filename;
            $uploadfile = $uploaddir . $filename;
            move_uploaded_file($item['tmp_name'], $uploadfile);
        }

        foreach ($filenames as $item) {
            $string = "insert ignore into users_content set filename = '".$item."' , path = '".ROOT."/uploads/".$item."' , id_homework = ".$homework_id." , date_send = NOW();";
            $db->query($string);
        }*/

        $db->query('update user_lessons set teacher_look = "0" , last_hw =  NOW() where id = {?}',
            array($id_user_lesson));

        echo_success('ok');
    } else {
        echo_error($update_count_try);
    }


}


/**
 * Загрузка файлов
 */
function uploadFile() {


            $uploaddir = '/academy/uploads/';
            $filename = generateRandomString(7). basename($_FILES['file']['name']);
            $uploadfile = $uploaddir . $filename;
            if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                echo_success(ROOT . "/uploads/" . $filename);
            } else {
                echo_error('Ошибка загрузки');
            }

}


/**
 * Функция получения чатов
 * */
function getChatCources()
{
    $id_user = $_POST['id_user'];
    $db = get_db();
    $results = $db->select('SELECT 
user_lessons.id, lessons.name as lesson_name, cources.name 
as cource_name FROM user_lessons 
LEFT JOIN lessons on user_lessons.id_lesson = lessons.id 
join cources on lessons.cource_id = cources.id 
WHERE user_lessons.id_user = {?} order by user_lessons.id AND cources.name ASC',
        array($id_user));

    $hash = $_COOKIE['hash'];
    $result = $db->select('SELECT admin , id FROM users where hash= {?}', array($hash));
    $admin_id = $result[0]['id'];

    for ($i=0;$i<count($results);$i++) {
        $res = $db->select('SELECT COUNT(id) as count_mes 
FROM messages 
WHERE messages.user_id <> {?} 
and messages.id_user_lesson = {?}
AND messages.status = 0',array($admin_id,$results[$i]['id']));
        $results[$i]['no_read'] = (int)$res[0]['count_mes'];
        $results[$i]['id_user'] = $id_user;
    }

    echo json_encode($results);
}


/**
 * Функция получения сообщений чатов
 * */
function getChatMessages($id_chat_room)
{

    $db = get_db();
    $hash = $_COOKIE['hash'];
    $user = $db->select('SELECT admin , id FROM users where hash= {?}', array($hash));
    $user_id = $user[0]['id'];

    $result = $db->select('SELECT 
messages.date_send, 
messages.user_id, messages.message, 
messages.id_user_lesson, users.username
from messages 
join users on messages.user_id = users.id
Where messages.id_user_lesson = {?}
Order by messages.date_send ASC',
        array($id_chat_room));


    $change_status = $db->query('UPDATE messages SET status = 1 where id_user_lesson = {?}',
        array($id_chat_room));


    for($i = 0; $i < count($result); $i++) {
        if ($result[$i]['user_id'] === $user_id) {
            $result[$i]['my'] = 1;
        } else {
            $result[$i]['my'] = 0;
        }
        $result[$i]['message'] = base64_decode($result[$i]['message']);
    }

    echo json_encode($result);
}

/**
 * Функция сохранения оценки за попытку
 * */
function setMarkToTry() {

    global $vk;

    $data = $_POST;
    $db = get_db();
    $update = $db->query('update homeworks set mark = {?} WHERE id = {?}',
        array($data['mark'], $data['id_homework']));

    $check = $db->select('select vk_notif, vk from users where id = {?}', array($data['id_user']));

    if ((int)$check[0]['vk_notif']) {
        $vk->setAccessToken('b15e70cce0714c754bb55f1e419638ee5f0d5e133d50bc2e35d3b5f62da41acd3845cc1954f794f44d6c2');

    $send_message = $vk->api('messages.send', array(
        'user_ids'   =>  $check[0]['vk'],
        'message' => 'Ваше домашнее задание по курсу '.$data['courcename'].' проверено с оценкой ' . $data['mark'] . ' баллов.'
    ));

    }

    if (is_bool($update) && $update) {
        echo_success('ok');
    } else {
        echo_error($update);
    }
}


/**
 * Функция открытия всех уроков курса для выбранного пользователя
 * */
function openAllLessonsForUser() {
    $data = $_POST;
    $db = get_db();

    $lessons = $db->select('select * from lessons where cource_id = {?}',array($data['cource_id']));
    foreach ($lessons as $value) {
        $db->query('update user_lessons set stage = "NEW" , pay = 1 where id_user = {?} AND id_lesson = {?}',
            array($data['id_user'], $value['id']));
    }

    echo_success('ok');
}


/**
 * Функция смены статуса непрочитано для домашки
 * */
function changeStatusHomeworkRead($id_user_lesson) {
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Истёк срок cookie');
        return;
    }

    $success = $db->query('update user_lessons set teacher_look = 0 where id = {?}',array($id_user_lesson));

    if ($success) {
        echo_success('ok');
    } else {
        echo_error('Не удалось обновить');
    }

}



/**
 * Функция сохранения настроек пользователя
 * */
function saveUserPayAndSetting() {

    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Истёк срок cookie');
        return;
    }

    $data = $_POST;
    $db = get_db();

    $user_setting = $data['userSetting'];
    $courses = $data['courses'];

    $saveSet = $db->query('UPDATE users SET 
mail={?} , 
username={?}, 
userlastname={?}, 
phone={?}, 
vk={?},
block = {?} WHERE id = {?}',
        array($user_setting['mail'], $user_setting['username'], $user_setting['userlastname'], $user_setting['phone'],
            $user_setting['vk'], $user_setting['block'],  $user_setting['id']));

    if (is_string($saveSet)) {
        echo_error($saveSet);
        return;
    }

    foreach ($courses as $course) {
        if ($course['his_cource'] === '0') {
            $db->query('delete from user_cources where id_cource = {?} AND id_user = {?}',
                array($course['id'],$user_setting['id']));
        }
        if ($course['his_cource'] === '1') {
            $count = $db->select('select count(id) as c from user_cources 
                                  WHERE id_user = {?} AND id_cource={?}',
                array($user_setting['id'],$course['id']));
            if ($count[0]['c'] === '0') {
                $db->query('insert into user_cources set id_user = {?} , id_cource={?}',
                    array($user_setting['id'],$course['id']));
            }
        }

        $lessons = $course['lessons'];
        foreach ($lessons as $lesson) {
            if ($lesson['pay'] === '0') {
                $db->query('update user_lessons set pay = 0 , stage = "CLOSE" where id = {?}', array( $lesson['id']) );
            } else {
                $db->query('update user_lessons set pay = 1 where id = {?}', array( $lesson['id']) );
            }
        }

        $two_firsy_lessons = $db->select('select user_lessons.id, user_lessons.stage, user_lessons.pay
from user_lessons 
join lessons on user_lessons.id_lesson = lessons.id 
WHERE lessons.cource_id = {?} and user_lessons.id_user = {?}
ORDER BY lessons.sort ASC
LIMIT 2', array($course['id'],$user_setting['id']));

        foreach ($two_firsy_lessons as $les) {
            if ($les['stage'] === 'CLOSE' && $les['pay'] === '1') {
                $db->query('update user_lessons set stage = "NEW" where id = {?}',array($les['id']));
            }
        }
    }

    echo_success('ok');

}


/**
 * Функция удаления вебинара
 * */
function delWebinars($id_webinar) {


    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Нет доступа');
        return;
    }



    // удаляем все сообщения
    $db->query('delete from webinars_message where id_webinar = {?}', array($id_webinar) );

    // удаляем вебинар
    $db->query('delete from webinars where id = {?}', array($id_webinar) );


    echo_success('Удалено!');
}

/**
 * Функция сохранения вебинаров
 * */
function saveWebinars() {


    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Нет доступа');
        return;
    }

    $webinars = $_POST['webinars'];


   foreach ($webinars as $webinar) {
       if (isset($webinar['id'])) {
           $db->query('update webinars set name = {?} , url = {?} where id = {?}', array($webinar['name'], $webinar['url'], $webinar['id']));
       } else {
           $result= $db->query('insert into webinars set webinars.name = {?} , url = {?} , video_url = {?} , artist = "none" ',
               array($webinar['name'], $webinar['url'] , 'https://www.youtube.com/embed/'));
       }
   }




    echo_success('Удалено!');
}


/**
 * Функция  удаления фейкового сообщения
 * */
function delWebinarMessage($id) {


    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Нет доступа');
        return;
    }

    $db->query('delete from webinars_message where id = {?}', array($id));

    echo_success('Удалено!');
}

/**
 * Функция  сохранения вебинара и сообщений
 * */
function saveWebinar() {


    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) return;
    } else {
        echo_error('Нет доступа');
        return;
    }

    $webinar = $_POST['webinar'];
    $messages = $_POST['messages'];





    $db->query('update webinars set webinars.name = {?} , 
                        time_start = {?} ,
                        artist = {?} ,
                        video_url = {?} ,
                        time_open_but = {?} ,
                        timer_close = {?} ,
                        start_video = {?},
                        end_video = {?}
                        WHERE id = {?}
                        ',
        array(
            $webinar['name'],
            $webinar['time_start'],
            $webinar['artist'],
            $webinar['video_url'],
            $webinar['time_open_but'],
            $webinar['timer_close'],
            $webinar['start_video'],
            $webinar['end_video'],
            $webinar['id']
           ));

    foreach ($messages as $message) {
        if (isset($message['id'])) {
            $db->query('update webinars_message 
                              set webinars_message.message = {?} ,
                              webinars_message.name = {?} ,
                              webinars_message.time = {?} 
                              WHERE id = {?}',
                array(
                    $message['message'],
                    $message['name'],
                    $message['time'],
                    $message['id']
                ));
        } else {
            $db->query('insert into webinars_message 
                              set webinars_message.message = {?} ,
                              webinars_message.name = {?} ,
                              webinars_message.time = {?} ,
                              webinars_message.id_webinar = {?}',
                array(
                    $message['message'],
                    $message['name'],
                    $message['time'],
                    $webinar['id']
                ));
        }
    }


    $ch = curl_init();


    curl_setopt($ch, CURLOPT_URL,"https://irs.academy:8098/setNewTime");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $server_output = curl_exec ($ch);

    curl_close ($ch);

    $ch = curl_init();


    curl_setopt($ch, CURLOPT_URL,"https://irs.academy:8098/setNewTimeOpen");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $server_output = curl_exec ($ch);

    curl_close ($ch);

    if ($server_output === 'ok' ) {
        echo_success('Сохранено!');
    } else {
        echo_error('Не удалось обновить сообщения!');
    }
}


/**
 * Функция отправки формы из вебинара
 * */
function sendWebinarModal() {
    $modalInfo = $_POST['modal'];

    if ($modalInfo['utmid'] === '0') {
        $modalInfo['utmid'] = 538;
    }
    $price = 5200;
    if ($modalInfo['option'] === 'rassrochka') {
        $price = 2500;
    }

    $invoice_ID = 0;



    if (isset($modalInfo['utm_content'])) {


        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.update.json';
        $queryData = http_build_query(array(
            'id' => $modalInfo['utm_content'],
            'fields' => array(
                "STAGE_ID" => 'C34:PREPAYMENT_INVOICE'
            ),
        ));
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        curl_exec($curl);
        curl_close($curl);


        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.get.json';
        $queryData = http_build_query(array(
            'id' => $modalInfo['utm_content']
        ));


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, 1);

        $contact_ID = $result['result']['CONTACT_ID'];

        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.invoice.add.json';
        $queryData = http_build_query(array(
            'fields' => array(
                "ORDER_TOPIC" => "Счёт за автовебинар",
                "STATUS_ID" => "N",
                "PERSON_TYPE_ID" => 12,
                "UF_CONTACT_ID" => $contact_ID,
                "UF_DEAL_ID" => $modalInfo['utm_content'],
                "UF_CRM_598C1BEC73D44" => "Ретушь",
                "PAY_SYSTEM_ID" => 12,
                "INVOICE_PROPERTIES"=> array(
                    "COMPANY"=> "Advanced Technology LLC",                               // Company name
                    "COMPANY_ADR"=> "687, New Broadway, London, A5 2XA",                 // Legal address
                    "REG_ID"=> "17863734634",                                            // Company registration number
                    "CONTACT_PERSON"=> "Jhon Smith",                                  // Contact person
                    "EMAIL"=> "pr@logistics-north.com",                                  // E-Mail
                    "PHONE"=> "8 (495) 234-54-32",                                       // Phone
                    "FAX"=> "",                                                          // Fax
                    "ZIP"=> "",                                                          // Postal code
                    "CITY"=> "",                                                         // City
                    "LOCATION"=> "",                                                     // Location
                    "ADDRESS"=> ""                                                       // Delivery address
                ),
                "PRODUCT_ROWS" => array(array(
                    "ID" => "40",
                    "PRODUCT_ID" => "40",
                    "QUANTITY" => "1",
                    "PRICE" => $price,
                )),
                "RESPONSIBLE_ID" => $modalInfo['utmid']
            ),
        ));
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $invoice_ID = json_decode($result, 1);
        $invoice_ID = $invoice_ID["result"];


    } else {

        $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.contact.add.json';

        $queryData = http_build_query(array(
            'fields' => array(
                "NAME" => $modalInfo['name']. ": вебинар " . $modalInfo['webname'],
                "LAST_NAME" => $modalInfo['last_name'],
                "OPENED" => "Y",
                "TYPE_ID" => "CLIENT",
                "SOURCE_ID" => "SELF",
                "PHONE" => array(array("VALUE" => $modalInfo['number'], "VALUE_TYPE" => "WORK" )),
                "EMAIL" => array(array("VALUE" => $modalInfo['email'], "VALUE_TYPE" => "WORK" )),
                "ASSIGNED_BY_ID" => $modalInfo['utmid'],
            ),
        ));


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $contact_ID = json_decode($result, 1);
        $contact_ID = $contact_ID["result"];



        if (is_numeric($contact_ID)) {

            $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.deal.add.json';
            $queryData = http_build_query(array(
                'fields' => array(
                    "TITLE" => $modalInfo['name']. ":" . $modalInfo['number'],
                    "CURRENCY_ID"=> "RUB",
                    "OPENED" => "Y",
                    "OPPORTUNITY" => $price,
                    "STAGE_ID" => 'C34:PREPAYMENT_INVOICE',
                    "CONTACT_ID" => $contact_ID,
                    "CATEGORY_ID" => 34,
                    "ASSIGNED_BY_ID" => $modalInfo['utmid']

                ),
            ));
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ));

            $result = curl_exec($curl);
            curl_close($curl);
            $deal_ID = json_decode($result, 1);
            $deal_ID = $deal_ID["result"];


            $queryUrl = 'https://irsib.bitrix24.ru/rest/1/23hc0snzv1knezy2/crm.invoice.add.json';
            $queryData = http_build_query(array(
                'fields' => array(
                    "ORDER_TOPIC" => "Счёт за автовебинар",
                    "STATUS_ID" => "N",
                    "PERSON_TYPE_ID" => 12,
                    "UF_CONTACT_ID" => $contact_ID,
                    "UF_DEAL_ID" => $deal_ID,
                    "UF_CRM_598C1BEC73D44" => "Ретушь",
                    "PAY_SYSTEM_ID" => 12,
                    "INVOICE_PROPERTIES"=> array(
                        "COMPANY"=> "Advanced Technology LLC",                               // Company name
                        "COMPANY_ADR"=> "687, New Broadway, London, A5 2XA",                 // Legal address
                        "REG_ID"=> "17863734634",                                            // Company registration number
                        "CONTACT_PERSON"=> "Jhon Smith",                                  // Contact person
                        "EMAIL"=> "pr@logistics-north.com",                                  // E-Mail
                        "PHONE"=> "8 (495) 234-54-32",                                       // Phone
                        "FAX"=> "",                                                          // Fax
                        "ZIP"=> "",                                                          // Postal code
                        "CITY"=> "",                                                         // City
                        "LOCATION"=> "",                                                     // Location
                        "ADDRESS"=> ""                                                       // Delivery address
                    ),
                    "PRODUCT_ROWS" => array(array(
                        "ID" => "40",
                        "PRODUCT_ID" => "40",
                        "QUANTITY" => "1",
                        "PRICE" => $price,
                    )),
                    "RESPONSIBLE_ID" => $modalInfo['utmid']
                ),
            ));
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ));

            $result = curl_exec($curl);
            curl_close($curl);
            $invoice_ID = json_decode($result, 1);
            $invoice_ID = $invoice_ID["result"];
    }

    }

    $db = get_db();

    $db->query('insert into who_send_webinar 
                              set name = {?} ,
                              phone = {?} ,
                              email = {?} ,
                              last_name = {?}',
        array(
            $modalInfo['name'],
            $modalInfo['number'],
            $modalInfo['email'],
            $modalInfo['last_name']
        ));

    echo_success(array(
        'ShopID' => '147485',
        'scid' => '106118',
        "customerNumber" => "12",
        "orderNumber" => $invoice_ID ,
        "Sum" => $price ,
        "paymentType" => "" ,
        "cms_name" =>"1C-Bitrix",
        "BX_HANDLER" => "YANDEX",
        "BX_PAYSYSTEM_CODE" => "12"
    ));

    //echo_success('https://money.yandex.ru/eshop.xml?scid=106118&sum='.$price.'&shopId=147485&customerNumber=12&orderNumber='.$invoice_ID.'/1&cms_name=1C-Bitrix&BX_HANDLER=YANDEX_REFERRER&BX_PAYSYSTEM_CODE=12');

}

/**
 * Функция частного случая
 */
function chSl() {
//    $db = get_db();
//    $users = $db->select('select id_user from user_cources where id_cource = {?}', array(4));
//    $lessons = $db->select('select id from lessons where cource_id = {?}',array(4));
//    foreach ($users as $user) {
//        foreach ($lessons as $lesson) {
//            $db->query('update user_lessons set stage = "NEW" , pay = 1
//                where id_lesson = {?} and id_user = {?}', array($lesson['id'], $user['id_user']));
//        }
//    }
//
//    echo 'ok';
}

/**
 * Функция проверки, выбирал ли пользователь способы получения уведомлений
 */
function checkNotif() {
    $db = get_db();
    $hash = $_COOKIE['hash'];

    if ($hash) {
        $res = $db->select('select check_form , admin from users where hash = {?}', array($hash));

        if ($res[0]['check_form']) {
            echo_success($res[0]['check_form']);
        } else {
            echo_error('not select');
        }
    } else {
        echo_success('not authorize');
    }

}

/**
 * Функция сохранения формы
 */
function saveCheckForm() {
    $db = get_db();
    $hash = $_COOKIE['hash'];
    $select = $_POST['select'];



    if ($hash) {
        $res = $db->select('select check_form , admin from users where hash = {?}', array($hash));

        switch ($select) {
            case 'VK': {
                $res = $db->query('update users set vk_notif = 1 , check_form = 1 where hash = {?}', array($hash));
                break;
            }
            case 'MAIL': {
                $res = $db->query('update users set mail_notif = 1 , check_form = 1 where hash = {?}', array($hash));
                break;
            }
            case 'MAIL_VK': {
                $res = $db->query('update users set vk_notif = 1 , mail_notif = 1 , check_form = 1 where hash = {?}', array($hash));
                break;
            }
        }

        if (is_bool($res)) {
            echo_success('ok');
            return;
        } else {
            echo_error($res);
        }

    } else {
        echo_success('not authorize');
    }

}