<?

function admin_user_id($user_id) {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }


        $db = get_db();
        $result = $db->select('SELECT id, username, 
mail,userlastname, phone,vk, notifdz, 
notifmes, notifnews, notifend, notifreport, admin, block FROM users where id = {?}', array($user_id));

        $cources = $db->select('SELECT id, name FROM cources');
        $lessons = $db->select('select * from lessons');

        foreach ($lessons as $lesson) {
            $count_user_less = $db->select('SELECT COUNT(id) as c FROM user_lessons 
WHERE user_lessons.id_lesson = {?} 
AND user_lessons.id_user = {?}', array($lesson['id'], $user_id));

            if ($count_user_less[0]['c'] === '0') {

                $db->query('insert ignore into user_lessons SET 
                            id_lesson = {?} ,
                            id_user = {?} ,
                            stage = "CLOSE",
                            send_files = 0,
                            content = "",
                            mark = 0,
                            teacher_look = 0,
                            pay = 0
                            ', array($lesson['id'],$user_id ));

            }
        }

        for ( $i = 0; $i < count($cources); $i++ ) {
            $his_cources_count = $db->select('select COUNT(id) as c from user_cources 
                                where user_cources.id_cource = {?} and user_cources.id_user = {?}',
                array( $cources[$i]['id'] ,$user_id ));


            if ((int)$his_cources_count[0]['c'] === 0 ) {
                $cources[$i]['his_cource'] = 0;
            } else {
                $cources[$i]['his_cource'] = 1;
            }

            $lessons =  $db->select('SELECT user_lessons.id , lessons.name, pay, sort FROM user_lessons 
join lessons on user_lessons.id_lesson = lessons.id 
WHERE user_lessons.id_user = {?} AND lessons.cource_id = {?}', array($user_id, $cources[$i]['id']));

            $cources[$i]['lessons'] = $lessons;

        }




    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => $result[0]['username'] . ' ' . $result[0]['userlastname'] ,
            'username' => $username
        )
    );
    echo loadView('templates/admin_user_id.php',array(
        'data_user'=> $result[0],
        'cources' => $cources
    ));
    echo loadView('templates/footer.php',array(
        'admin_ng' => true
    ));
}