var mysql = require('mysql');
var http = require('http');
var express = require('express'),
    app = module.exports.app = express();
var moment = require('moment');    

var server = http.createServer(app);
var io = require('socket.io').listen(server);  //pass a http.Server instance
server.listen(8090); 



var pool = mysql.createPool({
  host     : 'localhost',
  port 	   : '/var/run/mysqld/mysqld.sock', //port: '/Applications/MAMP/tmp/mysql/mysql.sock'
  user     : 'irsibnsk_newdb',
  password : 'BX9Oi5C;-C',
  database : 'irsibnsk_newdb'
});

app.get('/', function (req, res) {});

var usernames = {};

io.sockets.on('connection', function (socket) {

	socket.on('adduser', function(username, idroom, iduser){

	
		socket.username = username;

		socket.room = 'room' + idroom;

		socket.id_user_lesson = idroom;

		socket.iduser = iduser;
		
		usernames[iduser] = username;

		socket.join('room' + idroom);
		
		
	});

	socket.on('sendchat', function (message) {

		

		pool.getConnection(function(err,connection){
         if (err) {
           //callback(false);
			 console.log('Ошибка соединения:'+ err);
           return;
         }
         var datenow = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
         var set_data = {
         	'date_send': datenow, 
         	'status'   : 0, 
         	'message'  : new Buffer(message).toString('base64'),
         	'id_user_lesson' : socket.id_user_lesson,
         	'user_id'  : socket.iduser
         };
    	 connection.query("insert into messages set ?", set_data ,function(error, results, fields){
            connection.release();
            if(!err) {
            	console.log('результат:' + results);
              	//callback(true);
            } else {
            	console.log('Ошибка добавления:' + err);
            }
         });
     	 connection.on('error', function(err) {
              console.log(err);
              return;
         });
    	});
		var datenow = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
		io.sockets.in(socket.room).emit('updatechat', socket.username, message, datenow, socket.iduser);
		
	});
 
	socket.on('switchRoom', function(newroom){

		socket.leave(socket.room);
		
		socket.join('room' + newroom);
		
		//socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username+' has left this room');

		socket.room = 'room' + newroom;

		socket.id_user_lesson = newroom;

	});

	socket.on('disconnect', function(){
		
		delete usernames[socket.iduser];

		//io.sockets.emit('updateusers', usernames);
		
		socket.leave('room' +socket.room);
	});
});

console.log('Сервер запущен!Ты опущен!');