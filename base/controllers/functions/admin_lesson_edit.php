<?

function lesson_edit($id_lesson) {
    $username = chech_auth();
    $hash = $_COOKIE['hash'];

    if (isset($hash)) {
        $db = get_db();
        $result = $db->select('SELECT admin FROM users where hash= {?}', array($hash));
        if (!$result[0]['admin']) {header('Location: '.ROOT.'/setting'); die;}
    } else {
        header('Location: '.ROOT.'/auth');
        die();
    }

    $lesson = $db->select('Select * from lessons where id = {?}', array($id_lesson));

    $videos = $db->select('select * from lesson_videos where id_lesson = {?}', array($lesson[0]['id']));

    if (count($videos) === 0) {
        $videos = '';
    }

    $lesson[0]['video'] = $videos;


    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Редактировать урок '.$lesson[0]['name'],
            'username' => $username
        )
    );
    echo loadView('templates/admin_lesson_edit.php',array(
        'lesson'=> $lesson[0]
    ));
    echo loadView('templates/footer.php',array());
}