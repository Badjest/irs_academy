<?

function my() {

    $username = chech_auth();

    $hash = $_COOKIE['hash'];
    $db = get_db();
    $user_id = $db->select('select id from users where hash={?}',array($hash));
    $user_id = $user_id[0]['id'];
    $result = $db->select('SELECT cources.name as name, 
    cources.id as id, cources.photo as photo, 
    count(lessons.id) as count_lessons FROM user_cources 
    left join cources on user_cources.id_cource = cources.id 
    JOIN users on user_cources.id_user = users.id JOIN 
    lessons on lessons.cource_id = cources.id where users.hash = {?} GROUP BY user_cources.id_cource', array($hash));

    for ($i=0; $i < count($result); $i++) { 
        $all_count = $db->select('select count(id) as c from lessons where cource_id = {?}',array($result[$i]['id']));
        $all_count = $all_count[0]['c'];
        $complete_count = $db->select('Select COUNT(user_lessons.id) as c
                                        FROM user_lessons 
                                        JOIN lessons on user_lessons.id_lesson = lessons.id
                                        WHERE user_lessons.stage = "ACCESS" 
                                        AND user_lessons.id_user = {?} 
                                        AND lessons.cource_id = {?}', 
                                            array($user_id, $result[$i]['id']));
        $result[$i]['complete_lesson'] = $complete_count[0]['c'];
        $result[$i]['all_lesson'] = $all_count;
    }


    echo loadView('templates/header.php',
        array(
            'show_header'=>true,
            'title' => 'Мои курсы',
            'username' => $username
        )
    );
    echo loadView('templates/courses.php',array(
        'cources' => $result
    ));
    echo loadView('templates/footer.php',array());
}